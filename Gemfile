# frozen_string_literal: true

source 'https://rubygems.org'

ruby '3.1.4'

gem 'rails', '~> 6.1.7.1'

gem 'rack', '~> 2.2.4'

# DBs
gem 'pg', '~> 1.1'
gem 'redis-rails', '~> 5.0.2'

# Application server
gem 'unicorn-rails'

# Authentication libraries
gem 'devise', '~> 4.9.0'
gem 'omniauth-gitlab', '~> 4.1.0'
gem 'omniauth-rails_csrf_protection', '~> 1.0.1'

# API
gem 'jbuilder', '~> 2.0'
gem 'grape', '~> 1.8.0'
gem 'grape-entity', '~> 0.10.0'
gem 'rack-cors', '~> 1.1.1', require: 'rack/cors'

# Pagination
gem 'kaminari', '~> 1.2.1'

# HAML
gem 'hamlit', '~> 2.16.0'

# Misc
gem 'addressable'
gem 'public_suffix'
gem 'version_sorter', '~> 2.2'
gem 'gon'
gem 'gitlab-sdk', '~> 0.2'

# Assets
gem 'sass-rails', '~> 5.1.0'
gem 'turbolinks', '~> 5.2.0'
gem 'jquery-rails', '~> 4.6.0'
gem 'jquery-ui-rails'
gem 'nprogress-rails'
gem 'terser', '~> 1.2.0'

gem 'httparty'
gem 'sidekiq', '~> 6.1'
gem 'sidekiq-failures'
gem 'sidekiq_alive'
gem 'gitlab-sidekiq-fetcher', '0.9.0', require: 'sidekiq-reliable-fetch'
gem 'redis-namespace'

# Charts
gem 'groupdate', '~> 5.2.0'
gem 'chartkick', '~> 5.1.0'

# Twitter bootstrap
gem 'bootstrap', '~> 5.3.0'
gem 'dartsass-sprockets', '~> 3.1.0' # Sass engine for Bootstrap

gem 'nokogiri', '~> 1.16.0'

# Salesforce integration
gem 'restforce', '~> 3.2.0'

group :development do
  # The version used here should match the SAST analyzer for consistency
  # https://gitlab.com/gitlab-org/security-products/analyzers/brakeman/-/blob/master/Dockerfile#L28
  gem 'brakeman', '6.2.2', require: false
  gem 'web-console', '~> 3.7'

  # Docs generator
  gem "sdoc", '~> 1.1.0'
end

# Sentry integration
gem 'sentry-ruby', '~>5.18.0'
gem 'sentry-rails', '~>5.18.0'

group :development, :test do
  gem 'dotenv-rails'
  gem 'bundler-audit', '~> 0.9.1', require: false
  gem 'gitlab-dangerfiles', '~> 4.8.0', require: false
  gem 'git', '~> 1.19.0' # make danger use a higher version of this gem
  gem 'gitlab-styles', '~> 13.0.0', require: false
  gem 'lefthook', '~> 1.8.0', require: false

  gem 'rubocop', '~> 1.67.0'

  # Tests
  gem 'rspec-rails'
  gem 'minitest'
  gem 'launchy'

  # Profiling
  gem 'rack-mini-profiler', require: false

  gem 'spring', '1.7.2'

  # Generate Fake data
  gem 'faker'

  gem 'simplecov', require: false
  gem 'factory_bot_rails', '~> 6.4.0'

  # JS Runtime
  gem 'execjs'

  # Debugging
  gem 'pry-rails'
  gem 'pry-rescue'
  gem 'pry-stack_explorer'
  gem 'pry-nav'

  gem 'timecop', '~> 0.9.0'
end

group :test do
  gem 'puma', '~> 6.5.0' # for browser testing locally
  gem 'webmock', '~> 3.24.0'
  gem 'shoulda-matchers', '~> 6.4.0', require: false
  gem 'rspec-parameterized', require: false
  gem 'selenium-webdriver', '~> 3.142'
  gem 'webdrivers'
  gem 'capybara', '~> 3.40.0'
  gem 'capybara-screenshot', '~> 1.0.22'
  gem 'test-prof', '~> 0.12.0'
end
