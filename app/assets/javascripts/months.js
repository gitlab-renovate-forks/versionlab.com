function updateSubmitValue() {
  var submit = $('.js-month-submit');

  var totalCount = $('.js-month-checkbox:checked').
      map(function() { return parseInt(this.dataset.count); }).
      get().
      reduce(function(a, b) { return a + b; }, 0);

  submit.val('Schedule backfill for ' + totalCount + ' service pings');
}

updateSubmitValue();

$('.js-month-checkbox').change(updateSubmitValue);
