document.addEventListener("DOMContentLoaded", function () {
  if (!window.gon.browser_sdk_enabled) return;

  const glClient = window.glSDK.glClientSDK({
    appId: window.gon.browser_sdk_app_id || "version-app",
    host: window.gon.browser_sdk_host || "http://localhost:9090",
  });

  glClient.page();
});
