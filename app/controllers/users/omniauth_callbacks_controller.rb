# frozen_string_literal: true

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def gitlab
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      sign_in_and_redirect @user, event: :authentication
      set_flash_message(:notice, :success, kind: "GitLab") if is_navigational_format?
    else
      session["devise.gitlab_data"] = request.env["omniauth.auth"]
      redirect_to root_url
    end
  end
end
