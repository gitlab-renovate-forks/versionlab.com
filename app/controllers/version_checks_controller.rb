# frozen_string_literal: true

class VersionChecksController < ApplicationController
  LAST_VERSIONS_SHOWN_COUNT = 3

  skip_before_action :authenticate_user!, only: :check

  def check
    service = VersionCheckService.new(params[:gitlab_info])
    version_check_result = service.execute

    respond_to do |format|
      format.png do
        send_file File.realpath("public/update_#{version_check_result[:status]}.png"),
                  type: 'image/png', disposition: 'inline'
      end

      format.svg do
        send_file File.realpath("public/update_#{version_check_result[:status]}.svg"),
                  type: 'image/svg+xml', disposition: 'inline'
      end

      format.json do
        if version_check_result[:status] == 'failed'
          render json: {}, status: :bad_request
        else
          latest_version = Version.latest
          render json: {
            latest_stable_versions: latest_stable_versions,
            latest_stable_version_of_minor: latest_stable_version_of_minor,
            latest_version: latest_version.version,
            severity: version_check_result[:status],
            critical_vulnerability: version_check_result[:critical_vulnerability],
            details: latest_version.details
          }
        end
      end
    end
  end

  private

  def current_version
    @current_version ||= GitlabInfoDecoder.decode(params[:gitlab_info])['version']
  end

  def latest_stable_version_of_minor
    latest = Version.latest_for_minor_version(current_version)

    # If the latest version is vulnerable then it is not considered stable
    # and shouldn't be provided as an option for the customer.
    latest.version if latest&.vulnerability_type == 'not_vulnerable'
  end

  def latest_stable_versions
    Version.latest_minor_releases_after(
      count: LAST_VERSIONS_SHOWN_COUNT,
      after_version: current_version
    ).map(&:version)
  end
end
