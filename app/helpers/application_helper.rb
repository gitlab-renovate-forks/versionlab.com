# frozen_string_literal: true

module ApplicationHelper
  def nav_link(link_text, link_path)
    tag.li(class: 'nav-item') do
      class_name = current_page?(link_path) ? 'nav-link active' : 'nav-link'
      link_to link_text, link_path, class: class_name
    end
  end

  def sortable(column:, title: nil, default_order: 'asc')
    title, column, direction, css_class = calculate_sorting(column, title, default_order)

    link_to title, { sort: column, direction: direction }, { class: css_class }
  end

  def calculate_sorting(column, title, direction)
    title ||= column.titleize
    css_class = nil

    # already sorted one way, change to the other way
    if column == sort_column
      css_class = "current #{sort_direction}"
      direction = sort_direction == 'asc' ? 'desc' : 'asc'
    end

    [title, column, direction, css_class]
  end
end
