# frozen_string_literal: true

class RawUsageData < ApplicationRecord
  validates :payload, :uuid, :recorded_at, presence: true

  has_one :usage_data
end
