# frozen_string_literal: true

class CalculateConvIndexService
  GROWTH_METRICS = %w[
    boards
    ci_pipelines
    deployments
    environments
    issues
    merge_requests
    milestones
    notes
    service_desk_issues
  ].freeze

  PERIOD_DURATION = 35.days.freeze

  def execute(uuid, end_date)
    usage_records = fetch_usage_records(uuid, end_date)

    if usage_records.size > 1
      [true, calculate_metrics(usage_records)]
    else
      [false, {}]
    end
  end

  private

  def fetch_usage_records(uuid, end_date)
    start_date = end_date - PERIOD_DURATION
    UsageData.by_uuid_with_recorded_limits(uuid, start_date..end_date).to_a
  end

  def calculate_metrics(usage_records)
    oldest_record = usage_records.first
    newest_record = usage_records.last
    growth_metrics = calculate_growth_metrics(oldest_record, newest_record)
    prometheus_metrics = calculate_prometheus_metrics(newest_record)

    growth_metrics.merge(prometheus_metrics)
  end

  def calculate_growth_metrics(oldest_record, newest_record)
    GROWTH_METRICS.index_with do |field|
      calculate_growth_metric(oldest_record, newest_record, field)
    end
  end

  def calculate_prometheus_metrics(newest_record)
    projects = [get_value(newest_record, 'projects'), 1].max
    active = get_value(newest_record, 'projects_prometheus_active')
    value = active / projects

    { 'projects_prometheus_active' => value }
  end

  def calculate_growth_metric(oldest, newest, field)
    newest_value = get_value(newest, field)
    oldest_value = get_value(oldest, field)
    difference = [newest_value - oldest_value, 0].max

    difference / active_user_count(oldest).to_f
  end

  def get_value(record, field)
    stats = record.stats.to_h

    # no ci_pipelines since 9.4 (see 4447006832d8), try to handle the new format
    if field == 'ci_pipelines' && !stats.has_key?(field)
      get_value(record, 'ci_internal_pipelines') + get_value(record, 'ci_external_pipelines')
    else
      stats.fetch(field, 0).to_f
    end
  end

  def active_user_count(record)
    [record.active_user_count.to_i, 1].max
  end
end
