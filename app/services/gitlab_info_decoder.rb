# frozen_string_literal: true

class GitlabInfoDecoder
  def self.decode(gitlab_info)
    gitlab_info = CGI.unescape(gitlab_info)
    gitlab_info = Base64.urlsafe_decode64(gitlab_info)
    JSON.parse(gitlab_info)
  rescue # rubocop:disable Style/RescueStandardError
    log_info = "#{name}: Gitlab info not decoded"
    Rails.logger.warn(log_info)
    nil
  end
end
