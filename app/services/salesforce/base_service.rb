# frozen_string_literal: true

module Salesforce
  class BaseService
    POOL = ConnectionPool.new(size: 10, timeout: 60) do
      restforce_config = {
        username: Rails.application.secrets.salesforce_username,
        password: Rails.application.secrets.salesforce_password,
        security_token: Rails.application.secrets.salesforce_security_token,
        client_id: Rails.application.secrets.salesforce_client_id,
        client_secret: Rails.application.secrets.salesforce_client_secret,
        api_version: '38.0',
        host: Rails.configuration.application.salesforce_host
      }

      if Rails.env.development?
        restforce_config[:instance_url] = Rails.configuration.application.salesforce_instance_url
      end

      Restforce.new(restforce_config)
    end

    private

    def query(soql)
      Rails.logger.info('Salesforce API: #query')
      POOL.with { |client| client.query(soql) }
    end

    def create!(table, params)
      Rails.logger.info('Salesforce API: #create!')
      POOL.with { |client| client.create!(table, params) }
    end

    # https://developer.salesforce.com/docs/atlas.en-us.soql_sosl.meta/soql_sosl/sforce_api_calls_soql_select_quotedstringescapes.htm
    def quote(value)
      "'#{value.gsub(/(['"\\])/, '\\\\\1')}'"
    end

    def date_field(value)
      # https://help.salesforce.com/articleView?id=000240801&type=1
      return '' unless value && value.year < 4001

      if value.year < 100
        value.iso8601.sub('00', '20')
      else
        value.iso8601
      end
    end

    def json_field(value)
      value ? value.to_json : ''
    end
  end
end
