# frozen_string_literal: true

module UsagePing
  class FixParams
    SORTED_DATA_CATEGORIES = %w[Standard Subscription Operational].sort

    DEFAULT_VERSION = '14.1'
    FALLBACK_VALUE = -1

    def execute(request_params)
      @params = request_params

      fix_edition_and_version
      fix_gitaly_fallback
      fix_legacy_git_version
      fix_fallback_licensee

      params
    end

    private

    attr_reader :params

    def fix_fallback_licensee
      return unless params[:licensee] == -1

      params[:licensee] = {
        Name: -1,
        Company: -1,
        Email: -1
      }
    end

    def fix_legacy_git_version
      version = params.dig(:git, :version)
      return unless version.is_a?(String)

      version.split('.').tap do |v|
        params[:git][:version] = {
          major: v[0].to_i,
          minor: v[1].to_i,
          patch: v[2].to_i
        }
      end
    end

    # When Service ping payload:
    #   collected_data_categories: [Standard, Subscription, Operational]
    #   edition and version keys are missing
    # Update:
    #   edition: EE
    #   version: 14.1
    # This bug is the result of having edition and version metrics set as Optional instead of Standard
    # https://gitlab.com/gitlab-org/gitlab/-/issues/337734
    def fix_edition_and_version
      collected_data_categories = params.dig(:settings, :collected_data_categories)

      return params unless collected_data_categories&.sort == SORTED_DATA_CATEGORIES

      unless edition_and_version?
        params[:edition] = UsageData::EE_EDITION
        params[:version] = DEFAULT_VERSION
      end

      params
    end

    # When Service ping payload:
    #   gitaly.filesystems is Integer `-1` instead of an Array
    # Update:
    #   gitaly.filesystems: ["-1"]
    # See https://gitlab.com/gitlab-services/version-gitlab-com/-/issues/436
    def fix_gitaly_fallback
      params[:gitaly][:filesystems] = ["-1"] if params.dig(:gitaly, :filesystems) == FALLBACK_VALUE
    end

    def edition_and_version?
      (params.keys.map(&:to_sym) & [:edition, :version]).any?
    end
  end
end
