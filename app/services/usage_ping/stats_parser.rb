# frozen_string_literal: true

module UsagePing
  # Even if we change the payload that GitLab EE is sending,
  # we need to handle both incorrect and correctly formatted stats forever to
  # support old GitLab installations.

  class StatsParser
    def initialize(stats)
      @stats = stats
    end

    def execute
      parse!

      {
        stats: stats
      }
    end

    private

    attr_reader :stats

    def parse!
      parse_invalid_stats if illegal_stats_format?
      parse_invalid_epics_stats if illegal_epics_stats_format?
      parse_invalid_nil_values
    end

    def illegal_stats_format?
      stats.key?(:user_preferences) || stats.key?(:operations_dashboard)
    end

    def illegal_epics_stats_format?
      stats.key?(:work_item_parent_links) || stats.key?(:work_item_related_links)
    end

    def parse_invalid_stats
      illegal_stats = @stats.extract!(:user_preferences, :operations_dashboard)
      @stats = flatten_invalid_stats(illegal_stats).merge(stats)
    end

    def parse_invalid_epics_stats
      illegal_stats = @stats.extract!(:work_item_parent_links, :work_item_related_links)
      @stats = flatten_invalid_epics_stats(illegal_stats).merge(stats)
    end

    def flatten_invalid_stats(params)
      {
        user_preferences_group_overview_details:
          params.dig(:user_preferences, :group_overview_details).to_i,
        user_preferences_group_overview_security_dashboard:
          params.dig(:user_preferences, :group_overview_security_dashboard).to_i,
        operations_dashboard_default_dashboard:
          params.dig(:operations_dashboard, :default_dashboard).to_i,
        operations_dashboard_users_with_projects_added:
          params.dig(:operations_dashboard, :users_with_projects_added).to_i
      }
    end

    def flatten_invalid_epics_stats(params)
      {
        work_item_parent_links_epic_parent:
          params.dig(:work_item_parent_links, :epic_parent).to_i,
        work_item_related_links_epics:
          params.dig(:work_item_related_links, :epics).to_i
      }
    end

    def parse_invalid_nil_values
      @stats[:epics_deepest_relationship_level] ||= 0
    end
  end
end
