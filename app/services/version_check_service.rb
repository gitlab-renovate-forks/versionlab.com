# frozen_string_literal: true

class VersionCheckService
  attr_reader :gitlab_info

  def initialize(gitlab_info)
    @gitlab_info = GitlabInfoDecoder.decode(gitlab_info)
  end

  def execute
    return { status: 'failed' } if gitlab_info.nil? || gitlab_info['error'].present?

    @gitlab_version = gitlab_info['version']

    { status: status, critical_vulnerability: critical_vulnerability? }
  end

  private

  def status
    if update_required?
      'danger'
    elsif up_to_date?
      'success'
    else
      'warning'
    end
  rescue NoMethodError
    'failed'
  end

  def critical_vulnerability?
    Version.critical_vulnerability?(general_gitlab_version)
  end

  def up_to_date?
    Version.latest?(general_gitlab_version)
  end

  def update_required?
    Version.update_required?(general_gitlab_version)
  end

  def general_gitlab_version
    @gitlab_version.sub(/-ee\Z/, '')
  end
end
