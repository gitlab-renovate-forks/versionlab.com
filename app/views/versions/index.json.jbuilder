# frozen_string_literal: true

json.array!(@versions) do |version|
  json.extract! version, :id, :version, :vulnerable
  json.url version_url(version, format: :json)
end
