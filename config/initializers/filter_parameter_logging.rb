# frozen_string_literal: true

# Be sure to restart your server when you modify this file.

# Configure sensitive parameters which will be filtered from the log file.
# Filter Service Ping params in order to limit size of logs
Rails.application.config.filter_parameters += [:password] + UsagePing::FILTERED_KEYS
