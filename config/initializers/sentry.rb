# frozen_string_literal: true

Sentry.init do |config|
  config.dsn = ENV['SENTRY_DSN']
  config.enabled_environments = %w[staging production]
  config.environment = ENV['SENTRY_ENV'] || Rails.env

  config.send_default_pii = true
  config.traces_sample_rate = 0.2
end
