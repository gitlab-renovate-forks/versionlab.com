# frozen_string_literal: true

SidekiqAlive.setup do |config|
  config.port = ENV['SIDEKIQ_ALIVE_PORT'] if ENV['SIDEKIQ_ALIVE_PORT'].present?
end
