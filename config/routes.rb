# frozen_string_literal: true

require 'api/api'
require 'sidekiq/web'

Rails.application.routes.draw do
  get 'statistics/show'
  resource :profile, only: [:show, :update] do
    member do
      put :reset_private_token
    end
  end

  resources :hosts, only: [:show, :update]
  resources :current_host_stats, only: :index
  resources :usage_data, only: [:new, :create, :show] do
    collection do
      get 'months'
      post 'schedule_backfill'
    end
  end

  resources :metrics, only: [:index, :show], constraints: { id: /[A-Za-z_0-9]+/ }
  resources :versions, except: :show
  resources :usage_ping_errors, only: [:create]
  resources :usage_ping_metadata, only: [:create]

  get 'check(.:format)',
      to: 'version_checks#check',
      defaults: { format: 'png' },
      constraints: { format: /png|svg|json/ }

  # API
  API::API.logger Rails.logger
  mount API::API => '/api'

  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks'
  }

  root 'versions#index'

  authenticate :user do
    mount Sidekiq::Web => '/sidekiq', as: :sidekiq
  end
end
