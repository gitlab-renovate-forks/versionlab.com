# frozen_string_literal: true

worker_processes Integer(ENV["WEB_CONCURRENCY"] || 3)
timeout 60
preload_app true

before_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn master intercepting TERM and sending myself QUIT instead' # rubocop:disable Rails/Output
    Process.kill 'QUIT', Process.pid
  end

  defined?(ActiveRecord::Base) &&
    ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to send QUIT' # rubocop:disable Rails/Output
  end

  defined?(ActiveRecord::Base) &&
    ActiveRecord::Base.establish_connection
end
