# frozen_string_literal: true

class AddRefererToCheck < ActiveRecord::Migration[4.2]
  def change
    add_column :version_checks, :referer_url, :string # rubocop:disable Migration/AddLimitToStringColumns
  end
end
