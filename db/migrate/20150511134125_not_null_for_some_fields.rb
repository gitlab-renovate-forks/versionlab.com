# frozen_string_literal: true

class NotNullForSomeFields < ActiveRecord::Migration[4.2]
  def change
    change_column :version_checks, :host_id, :integer, null: false # rubocop:disable Rails/BulkChangeTable
    change_column :version_checks, :referer_url, :string, null: false
    change_column :version_checks, :gitlab_version, :string, null: false
    change_column :hosts, :url, :string, null: false
    change_column :versions, :version, :string, null: false
  end
end
