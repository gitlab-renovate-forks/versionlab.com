# frozen_string_literal: true

class AddColumnsToUsers < ActiveRecord::Migration[4.2]
  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    add_column :users, :provider, :string # rubocop:disable Rails/BulkChangeTable
    add_column :users, :uid, :string
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
