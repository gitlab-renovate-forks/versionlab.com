# frozen_string_literal: true

class ActuallyRemoveDoubleHosts < ActiveRecord::Migration[4.2]
  def change
    grouped = Host.all.group_by { |host| URI.parse(host.url).host rescue nil } # rubocop:disable Style/RescueModifier
    doubles = 0 # rubocop:disable Lint/UselessAssignment

    grouped.values.each do |duplicates| # rubocop:disable Style/HashEachMethods
      first = duplicates.shift

      puts "removing #{duplicates.count} from #{first.url}"
      Host.delete(duplicates)

      new_url = URI.parse(first.url).host rescue nil # rubocop:disable Style/RescueModifier
      if new_url
        first.url = new_url
        first.save
      end
    end
  end
end
