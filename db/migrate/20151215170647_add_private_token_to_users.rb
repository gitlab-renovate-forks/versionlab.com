# frozen_string_literal: true

class AddPrivateTokenToUsers < ActiveRecord::Migration[4.2]
  def up
    add_column :users, :private_token, :string, index: { unique: true } # rubocop:disable Migration/AddLimitToStringColumns
  end

  def down
    remove_index :users, column: :private_token if index_exists?(:users, :private_token) # rubocop:disable Migration/RemoveIndex
    remove_column :users, :private_token
  end
end
