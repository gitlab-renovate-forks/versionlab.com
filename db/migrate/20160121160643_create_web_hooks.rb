# frozen_string_literal: true

class CreateWebHooks < ActiveRecord::Migration[4.2]
  def change
    create_table :web_hooks do |t|
      t.string :url, null: false # rubocop:disable Migration/AddLimitToStringColumns
      t.boolean :active, null: false, default: false

      t.timestamps null: false # rubocop:disable Migration/Timestamps
    end
  end
end
