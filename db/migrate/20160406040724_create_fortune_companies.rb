# frozen_string_literal: true

class CreateFortuneCompanies < ActiveRecord::Migration[4.2]
  def change
    create_table :fortune_companies do |t|
      t.integer :rank, null: false
      t.string :company, null: false # rubocop:disable Migration/AddLimitToStringColumns
      t.string :domain, null: false # rubocop:disable Migration/AddLimitToStringColumns

      t.timestamps null: false # rubocop:disable Migration/Timestamps
    end

    add_index :fortune_companies, :domain, unique: true
  end
end
