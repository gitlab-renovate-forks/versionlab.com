# frozen_string_literal: true

class AddFortuneToHosts < ActiveRecord::Migration[4.2]
  def change
    add_column :hosts, :fortune_rank, :integer
  end
end
