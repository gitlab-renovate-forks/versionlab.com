# frozen_string_literal: true

class CreateLicenses < ActiveRecord::Migration[4.2]
  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    create_table :licenses do |t|
      t.string :name
      t.string :email
      t.string :company
      t.integer :user_count
      t.string :add_ons
      t.string :md5
      t.date :starts_on
      t.date :expires_on
      t.timestamps null: false # rubocop:disable Migration/Timestamps
    end
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
