# frozen_string_literal: true

class AddLicenseIdToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_reference :usage_data, :license, index: true, foreign_key: { on_delete: :cascade } # rubocop:disable Migration/AddReference
  end
end
