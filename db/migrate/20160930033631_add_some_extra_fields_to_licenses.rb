# frozen_string_literal: true

class AddSomeExtraFieldsToLicenses < ActiveRecord::Migration[4.2]
  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    add_column :licenses, :active_users_count, :integer # rubocop:disable Rails/BulkChangeTable
    add_column :licenses, :historical_max_users_count, :integer
    add_column :licenses, :last_ping_received_at, :datetime # rubocop:disable Migration/Datetime
    add_column :licenses, :version, :string
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
