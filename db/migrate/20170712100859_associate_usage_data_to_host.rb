# frozen_string_literal: true

class AssociateUsageDataToHost < ActiveRecord::Migration[4.2]
  def up
    # no-op, was to do a one time data fix in production
  end

  def down
    # no-op, was to do a one time data fix in production
  end
end
