# frozen_string_literal: true

class AddLicenseTrialToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :license_trial, :boolean
  end
end
