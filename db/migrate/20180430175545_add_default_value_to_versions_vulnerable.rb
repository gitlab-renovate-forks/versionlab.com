# frozen_string_literal: true

class AddDefaultValueToVersionsVulnerable < ActiveRecord::Migration[4.2]
  def change
    change_column_default :versions, :vulnerable, false # rubocop:disable Rails/ReversibleMigration
  end
end
