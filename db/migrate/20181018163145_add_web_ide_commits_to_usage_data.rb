# frozen_string_literal: true

class AddWebIdeCommitsToUsageData < ActiveRecord::Migration[5.0]
  def change
    add_column :usage_data, :web_ide_commits, :integer
  end
end
