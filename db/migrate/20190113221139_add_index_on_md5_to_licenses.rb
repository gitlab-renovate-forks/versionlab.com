# frozen_string_literal: true

class AddIndexOnMd5ToLicenses < ActiveRecord::Migration[5.0]
  def change
    add_index :licenses, :md5 # rubocop:disable Migration/AddIndex
  end
end
