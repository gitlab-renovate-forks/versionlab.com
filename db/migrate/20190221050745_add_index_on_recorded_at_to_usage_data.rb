# frozen_string_literal: true

class AddIndexOnRecordedAtToUsageData < ActiveRecord::Migration[5.0]
  disable_ddl_transaction!

  def change
    add_index :usage_data, :recorded_at, algorithm: :concurrently # rubocop:disable Migration/AddIndex
  end
end
