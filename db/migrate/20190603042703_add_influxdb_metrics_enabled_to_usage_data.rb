# frozen_string_literal: true

class AddInfluxdbMetricsEnabledToUsageData < ActiveRecord::Migration[5.0]
  def change
    add_column :usage_data, :influxdb_metrics_enabled, :boolean
  end
end
