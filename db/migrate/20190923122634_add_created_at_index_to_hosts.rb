# frozen_string_literal: true

class AddCreatedAtIndexToHosts < ActiveRecord::Migration[5.0]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_concurrent_index :hosts, :created_at
  end

  def down
    remove_index :hosts, :created_at if index_exists?(:hosts, :created_at) # rubocop:disable Migration/RemoveIndex
  end
end
