# frozen_string_literal: true

class AddCreatedAtIndexToVersionChecks < ActiveRecord::Migration[5.0]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_concurrent_index :version_checks, :created_at
  end

  def down
    remove_index :version_checks, :created_at if index_exists?(:version_checks, :created_at) # rubocop:disable Migration/RemoveIndex
  end
end
