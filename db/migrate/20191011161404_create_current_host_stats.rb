# frozen_string_literal: true

class CreateCurrentHostStats < ActiveRecord::Migration[5.0]
  def change
    create_table :current_host_stats do |t|
      t.references :host, index: true, foreign_key: { on_delete: :cascade }, unique: true, null: false
      t.string :url, limit: 255, null: false
      t.string :version, limit: 255
      t.datetime :host_created_at, null: false
      t.datetime :version_check_created_at
      t.datetime :usage_data_recorded_at
      t.integer :active_users_count
      t.integer :historical_max_users_count
      t.integer :user_count

      t.timestamps # rubocop:disable Migration/Timestamps
    end

    add_index :current_host_stats, :url
    add_index :current_host_stats, :version
    add_index :current_host_stats, :host_created_at
    add_index :current_host_stats, :version_check_created_at
    add_index :current_host_stats, :usage_data_recorded_at
    add_index :current_host_stats, :active_users_count
    add_index :current_host_stats, :historical_max_users_count
    add_index :current_host_stats, :user_count
  end
end
