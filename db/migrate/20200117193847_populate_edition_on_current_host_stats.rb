# frozen_string_literal: true

class PopulateEditionOnCurrentHostStats < ActiveRecord::Migration[5.1]
  def up
    # no-op this was for a one time data movement in production of edition data into current host stat
    # the seeding process for local development should take care of this for future local dev concerns
  end

  def down
    # no-op
  end
end
