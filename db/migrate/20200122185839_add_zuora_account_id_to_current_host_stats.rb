# frozen_string_literal: true

class AddZuoraAccountIdToCurrentHostStats < ActiveRecord::Migration[5.1]
  def up
    add_column :current_host_stats, :zuora_account_id, :string, limit: 255
  end

  def down
    remove_column :current_host_stats, :zuora_account_id, :string
  end
end
