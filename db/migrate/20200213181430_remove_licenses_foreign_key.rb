# frozen_string_literal: true

class RemoveLicensesForeignKey < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    remove_concurrent_index :usage_data, column: :license_id if index_exists?(:usage_data, :license_id)
    remove_column :usage_data, :license_id, :integer
  end

  def down
    add_column :usage_data, :license_id, :integer
    add_concurrent_index :usage_data, :license_id # rubocop:disable Migration/UpdateLargeTable
  end
end
