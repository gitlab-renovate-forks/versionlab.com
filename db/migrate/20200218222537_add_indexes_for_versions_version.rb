# frozen_string_literal: true

class AddIndexesForVersionsVersion < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_concurrent_index :versions, :version, unique: true
  end

  def down
    remove_concurrent_index :versions, :version, unique: true
  end
end
