# frozen_string_literal: true

class CopyCountsToStats < ActiveRecord::Migration[5.2]
  def up
    # no-op this was for a one time data integrity issue in production for counts that may not be in stats
  end

  def down
    # no-op
  end
end
