# frozen_string_literal: true

class DropWebIdeCommitsFromUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  def change
    remove_column :usage_data, :web_ide_commits, :integer
  end
end
