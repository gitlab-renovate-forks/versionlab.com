# frozen_string_literal: true

class AddRawUsageDataIdToUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  def change
    add_column :usage_data, :raw_usage_data_id, :bigint
  end
end
