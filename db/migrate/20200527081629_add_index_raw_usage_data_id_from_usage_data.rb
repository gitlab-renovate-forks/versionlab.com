# frozen_string_literal: true

class AddIndexRawUsageDataIdFromUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_concurrent_index :usage_data, :raw_usage_data_id # rubocop:disable Migration/UpdateLargeTable

    add_concurrent_foreign_key :usage_data, :raw_usage_data, column: :raw_usage_data_id, on_delete: :nullify
  end

  def down
    remove_foreign_key_if_exists :usage_data, column: :raw_usage_data_id

    remove_concurrent_index :usage_data, :raw_usage_data_id
  end
end
