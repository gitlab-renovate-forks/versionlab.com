# frozen_string_literal: true

class AddIngressModsecurityEnabledToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :ingress_modsecurity_enabled, :boolean
  end
end
