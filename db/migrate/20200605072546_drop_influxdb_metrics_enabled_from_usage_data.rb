# frozen_string_literal: true

class DropInfluxdbMetricsEnabledFromUsageData < ActiveRecord::Migration[5.2]
  def change
    remove_column :usage_data, :influxdb_metrics_enabled, :boolean
  end
end
