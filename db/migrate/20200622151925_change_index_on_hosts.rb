# frozen_string_literal: true

class ChangeIndexOnHosts < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_concurrent_index :hosts, :url, unique: true, name: 'index_hosts_on_url_unique'
    remove_concurrent_index :hosts, :url, name: 'index_hosts_on_url'
  end

  def down
    add_concurrent_index :hosts, :url, unique: false, name: 'index_hosts_on_url'
    remove_concurrent_index :hosts, :url, name: 'index_hosts_on_url_unique'
  end
end
