# frozen_string_literal: true

class AddIndexCreatedAtToUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_concurrent_index :usage_data, :created_at # rubocop:disable Migration/UpdateLargeTable
  end

  def down
    remove_concurrent_index :usage_data, :created_at
  end
end
