# frozen_string_literal: true

class AddPrometheusEnabledToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :prometheus_enabled, :boolean
  end
end
