# frozen_string_literal: true

class AddContainerRegistryVendorAndVersionToUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  def change
    change_table :usage_data, bulk: true do |t|
      t.string :container_registry_vendor, limit: 255
      t.string :container_registry_version, limit: 255
    end
  end
end
