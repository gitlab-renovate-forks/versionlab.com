# frozen_string_literal: true

class RemoveAppServerTypeFromUsageData < ActiveRecord::Migration[5.2]
  def change
    remove_column :usage_data, :app_server_type, :string, limit: 255
  end
end
