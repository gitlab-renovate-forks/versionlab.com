# frozen_string_literal: true

class DeleteAugUsageDataRequests < ActiveRecord::Migration[5.2]
  def up
    # no-op
  end

  def down
    # no-op
  end
end
