# frozen_string_literal: true

class AddDetailsToVerion < ActiveRecord::Migration[6.1]
  def change
    add_column :versions, :details, :text
    add_check_constraint :versions, "char_length(details) < 2049",
                         name: 'versions_details_length_check'
  end
end
