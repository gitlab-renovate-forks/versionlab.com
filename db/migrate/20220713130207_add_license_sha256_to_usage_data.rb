# frozen_string_literal: true

class AddLicenseSha256ToUsageData < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers

  def change
    add_column :usage_data, :license_sha256, :string, limit: 64
  end
end
