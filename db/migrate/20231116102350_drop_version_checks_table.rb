# frozen_string_literal: true

class DropVersionChecksTable < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  TABLE_NAME = :version_checks

  def up
    drop_table TABLE_NAME
  end

  def down
    # rubocop:disable Migration/AddLimitToStringColumns
    unless table_exists?(TABLE_NAME)
      create_table TABLE_NAME, id: :serial do |t|
        t.text "request_data"
        t.datetime "created_at", null: false
        t.datetime "updated_at", null: false
        t.string "gitlab_version", null: false
        t.string "referer_url", null: false
        t.integer "host_id", null: false
      end
    end
    # rubocop:enable Migration/AddLimitToStringColumns
    add_concurrent_index TABLE_NAME, :created_at
    add_concurrent_index TABLE_NAME, :host_id
    add_concurrent_index TABLE_NAME, :updated_at
  end
end
