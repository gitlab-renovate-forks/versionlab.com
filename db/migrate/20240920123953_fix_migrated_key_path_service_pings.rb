# frozen_string_literal: true

class FixMigratedKeyPathServicePings < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers

  MIGRATION = 'FixMigratedKeyPathUsageData'
  DELAY_INTERVAL = 2.minutes
  START_DATE_ID  = 52098799 # first id for Gitlab::BackgroundMigration::FixMigratedKeyPathUsageData::START_DATE
  END_DATE_ID    = 54700614 # last id for Gitlab::BackgroundMigration::FixMigratedKeyPathUsageData::END_DATE

  class RawUsageData < ActiveRecord::Base
    include ::EachBatch

    self.table_name = 'raw_usage_data'
  end

  def up
    # TODO: this migration can be changed into a no-op after
    # https://gitlab.com/gitlab-org/gitlab/-/issues/486036 is closed
    queue_background_migration_jobs_by_range_at_intervals(
      RawUsageData.where(id: START_DATE_ID..END_DATE_ID),
      MIGRATION,
      DELAY_INTERVAL,
      batch_size: 10_000
    )
  end

  def down
    # no-op
  end
end
