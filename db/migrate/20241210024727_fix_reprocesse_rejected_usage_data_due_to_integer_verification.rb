# frozen_string_literal: true

# See http://doc.gitlab.com/ce/development/migration_style_guide.html
# for more information on how to write migrations for GitLab.

class FixReprocesseRejectedUsageDataDueToIntegerVerification < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers

  MIGRATION = 'ReprocessMistakenlyRejectedUsageDataRecords'
  DELAY_INTERVAL = 2.minutes
  START_DATE_ID  = 71531000 # first id for Gitlab::BackgroundMigration::FixMigratedKeyPathUsageData::START_DATE
  END_DATE_ID    = 72259900 # last id for Gitlab::BackgroundMigration::FixMigratedKeyPathUsageData::END_DATE

  class RawUsageData < ActiveRecord::Base
    include ::EachBatch

    self.table_name = 'raw_usage_data'
  end

  def up
    # no-op
  end

  def down
    # no-op
  end
end
