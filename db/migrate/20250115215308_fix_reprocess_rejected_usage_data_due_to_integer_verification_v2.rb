# frozen_string_literal: true

class FixReprocessRejectedUsageDataDueToIntegerVerificationV2 < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers

  MIGRATION = 'ReprocessMistakenlyRejectedUsageDataRecords'
  DELAY_INTERVAL = 2.minutes
  START_DATE_ID  = 58206670 # first id for Gitlab::BackgroundMigration::FixMigratedKeyPathUsageData::START_DATE
  END_DATE_ID    = 59599791 # last id for Gitlab::BackgroundMigration::FixMigratedKeyPathUsageData::END_DATE

  class RawUsageData < ActiveRecord::Base
    include ::EachBatch

    self.table_name = 'raw_usage_data'
  end

  def up
    # ToDo: remove when #508580 is closed

    queue_background_migration_jobs_by_range_at_intervals(
      RawUsageData.where(id: START_DATE_ID..END_DATE_ID),
      MIGRATION,
      DELAY_INTERVAL,
      batch_size: 10_000
    )
  end

  def down
    # no-op
  end
end
