# frozen_string_literal: true

class RemoveNotNullConstraintFromUsagePingErrorsMessage < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers

  def up
    change_column_null :usage_ping_errors, :message, true
  end

  def down
    change_column_null :usage_ping_errors, :message, false, 'Unknown error'
  end
end
