# Data models

## UsageData

### uuid

Represents uniq instance identifier.

However, as it was [discovered](https://gitlab.com/gitlab-org/gitlab/-/issues/519440) multiple instance can share the same
id when instances use pre-provisioned DB (in this case `uuid` was created during provisioning and store in the DB).


## RawUsageData

### uuid

Same as `UsageData.uuid`
