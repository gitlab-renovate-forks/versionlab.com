# frozen_string_literal: true

module API
  module Entities
    class Version < Grape::Entity
      expose :id
      expose :version
      expose :major
      expose :minor
      expose :vulnerable, if: ->(_, options) { options[:current_user].present? } do |record|
        !record.not_vulnerable?
      end
      expose :details

      expose :created_at
    end

    class Feature < Grape::Entity
      expose :title
      expose :description
      expose :applicable_roles
      expose :read_more_url
      expose :created_at
      expose :version

      private

      def version
        object.version.version
      end
    end
  end
end
