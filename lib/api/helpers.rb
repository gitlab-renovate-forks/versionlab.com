# frozen_string_literal: true

module API
  module Helpers
    PRIVATE_TOKEN_HEADER = "HTTP_PRIVATE_TOKEN"
    PRIVATE_TOKEN_PARAM = :private_token

    # rubocop:disable Gitlab/ModuleWithInstanceVariables
    def current_user
      return @current_user if defined?(@current_user)

      private_token = params[PRIVATE_TOKEN_PARAM] || env[PRIVATE_TOKEN_HEADER]
      @current_user = private_token.present? ? User.find_by(private_token: private_token.to_s) : nil # rubocop:disable CodeReuse/ActiveRecord
    end
    # rubocop:enable Gitlab/ModuleWithInstanceVariables

    # Whenever we present, we want to consistently provide the current_user to
    # the entity scope. This can be accessed as `options[:current_user]` within
    # entities.
    def present(resource, **options)
      super(resource, **options.merge(current_user: current_user))
    end

    def authenticate!
      unauthorized! unless current_user
    end

    def paginate(relation)
      per_page = params[:per_page].to_i
      per_page = relation.default_per_page if per_page.zero?
      paginated = relation.page(params[:page]).per(per_page)
      add_pagination_headers(paginated, per_page)

      paginated
    end

    # Checks the occurrences of required attributes, each attribute must be present in the params hash
    # or a Bad Request error is invoked.
    #
    # Parameters:
    #   keys (required) - A hash consisting of keys that must be present
    def required_attributes!(keys)
      keys.each do |key|
        bad_request!(key) unless params[key].present?
      end
    end

    def attributes_for_keys(keys, custom_params = nil)
      attrs = {}

      keys.each do |key|
        attrs[key] = params[key] if params_present?(key)
      end

      ActionController::Parameters.new(attrs).permit!
    end

    # error helpers

    def bad_request!(attribute)
      message = ["400 (Bad request)"]
      message << "\"" + attribute.to_s + "\" not given"
      render_api_error!(message.join(' '), 400)
    end

    def unauthorized!
      render_api_error!('401 Unauthorized', 401)
    end

    def render_validation_error!(model)
      return unless model.errors.any?

      render_api_error!(model.errors.messages || '400 Bad Request', 400)
    end

    def render_api_error!(message, status)
      error!({ 'message' => message }, status)
    end

    private

    def add_pagination_headers(paginated, per_page)
      request_url = request.url.split('?').first

      links = []

      unless paginated.first_page?
        links << %(<#{request_url}?page=#{paginated.current_page - 1}&per_page=#{per_page}>; rel="prev")
      end

      unless paginated.last_page?
        links << %(<#{request_url}?page=#{paginated.current_page + 1}&per_page=#{per_page}>; rel="next")
      end

      links << %(<#{request_url}?page=1&per_page=#{per_page}>; rel="first")
      links << %(<#{request_url}?page=#{paginated.total_pages}&per_page=#{per_page}>; rel="last")

      header 'Link', links.join(', ')
    end

    def params_present?(key)
      params[key].present? || (params.has_key?(key) && params[key] == false)
    end
  end
end
