# frozen_string_literal: true

module Gitlab
  module BackgroundMigration
    class DeleteUsageData
      class UsageData < ActiveRecord::Base
        self.table_name = 'usage_data'
      end

      class CurrentHostStat < ActiveRecord::Base
        self.table_name = 'current_host_stats'
      end

      class Host < ActiveRecord::Base
        self.table_name = 'hosts'
      end

      class AvgCycleAnalytics < ActiveRecord::Base
        self.table_name = 'avg_cycle_analytics'
      end

      class ConversationalDevelopmentIndex < ActiveRecord::Base
        self.table_name = 'conversational_development_indices'
      end

      class RawUsageData < ActiveRecord::Base
        self.table_name = 'raw_usage_data'
      end

      def perform(hostnames)
        UsageData.transaction do
          hostnames.each do |hostname|
            host = Host.find_by(url: hostname)

            next unless host

            usage_data = UsageData.where(host_id: host.id)
            current_host_stat = CurrentHostStat.where(host_id: host.id)

            remove_usage_data(usage_data)
            reset_host_stats(current_host_stat)
          end
        end
      end

      private

      def remove_usage_data(usage_data)
        usage_data.each do |usage_datum|
          AvgCycleAnalytics.where(usage_data_id: usage_datum.id).delete_all
          ConversationalDevelopmentIndex.where(usage_data_id: usage_datum.id).delete_all
          RawUsageData.where(id: usage_datum.raw_usage_data_id).delete_all
        end

        usage_data.delete_all
      end

      def reset_host_stats(current_host_stat)
        current_host_stat.update_all(
          usage_data_recorded_at: nil,
          active_users_count: nil,
          historical_max_users_count: nil,
          user_count: nil,
          license_md5: nil,
          edition: nil
        )
      end
    end
  end
end

