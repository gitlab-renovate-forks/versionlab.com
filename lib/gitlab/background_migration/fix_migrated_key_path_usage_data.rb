# frozen_string_literal: true

module Gitlab
  module BackgroundMigration
    class FixMigratedKeyPathUsageData
      # TODO: this migration can be removed after https://gitlab.com/gitlab-org/gitlab/-/issues/486036 is closed

      START_DATE = "2024-08-04".to_time # 1 day before error metrics were merged https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161331
      END_DATE = "2024-09-21".to_time # 1 day after fix has been deployed https://gitlab.com/gitlab-org/gitlab-services/version.gitlab.com/-/merge_requests/181
      SOURCE_IP = "127.0.0.1"

      # rubocop:disable Naming/InclusiveLanguage - use UsageDataController's language for clarity
      def perform(start_id, end_id)
        usage_param_keys = UsagePing::USAGE_DATA_PARAMS + UsagePing::FEATURES_PARAMS
        whitelisted_hash_keys = UsageDataController.new.send(:whitelisted_hash_keys)
        RawUsageData
          .where(
            id: start_id..end_id,
            created_at: START_DATE..END_DATE
          )
          .where("payload->'counts'->'work_item_parent_links' IS NOT NULL")
          .each do |raw_usage_data|
            handle_raw_usage_data(raw_usage_data, usage_param_keys, whitelisted_hash_keys)
          end
      end

      private

      def handle_raw_usage_data(raw_usage_data, usage_param_keys, whitelisted_hash_keys)
        # simulate re-sending the raw_usage_data payload to the UsageDataController:

        # simulate controller's #save_original_usage_ping
        filtered_payload = ActionController::Parameters.new(raw_usage_data.payload)
        new_raw_usage_data = RawUsageData.create(
          uuid: filtered_payload[:uuid],
          recorded_at: filtered_payload[:recorded_at],
          payload: filtered_payload,
          raw_usage_data_payload: raw_usage_data.raw_usage_data_payload
        )

        # simulate controller's #usage_data_params
        usage_data_params = filtered_payload.permit(*usage_param_keys).tap do |whitelisted|
          whitelisted_hash_keys.each do |key|
            whitelisted[key] = filtered_payload[key].permit! if filtered_payload[key].present?
          end

          whitelisted[:stats] = filtered_payload[:counts].permit! if filtered_payload[:counts].present?
        end

        UsagePing::ServiceHandler.new.execute(
          usage_data_params.to_h, SOURCE_IP, new_raw_usage_data.id
        )
      rescue StandardError
      end
      # rubocop:enable Naming/InclusiveLanguage
    end
  end
end
