# frozen_string_literal: true

module Gitlab
  module BackgroundMigration
    class ReprocessMistakenlyRejectedUsageDataRecords
      # TODO: this migration can be removed after
      # https://gitlab.com/gitlab-org/gitlab-services/version.gitlab.com/-/merge_requests/214 is closed

      START_DATE = "2024-11-20".to_time # 1 day before 17.6 release
      END_DATE = "2024-12-11".to_time # 1 day after fix has been deployed https://gitlab.com/gitlab-org/gitlab-services/version.gitlab.com/-/jobs/8592045155
      SOURCE_IP = "127.0.0.1"

      # rubocop:disable Naming/InclusiveLanguage - use UsageDataController's language for clarity
      def perform(start_id, end_id)
        usage_param_keys = UsagePing::USAGE_DATA_PARAMS + UsagePing::FEATURES_PARAMS
        whitelisted_hash_keys = UsageDataController.new.send(:whitelisted_hash_keys)
        RawUsageData
          .where(
            id: start_id..end_id,
            created_at: START_DATE..END_DATE
          )
          .where("payload->>'version' >= '17.6'")
          .each do |raw_usage_data|
          handle_raw_usage_data(raw_usage_data, usage_param_keys, whitelisted_hash_keys)
        end
      end

      private

      def handle_raw_usage_data(raw_usage_data, usage_param_keys, whitelisted_hash_keys)
        # simulate re-sending the raw_usage_data payload to the UsageDataController:

        filtered_payload = ActionController::Parameters.new(raw_usage_data.payload)

        # simulate controller's #usage_data_params
        usage_data_params = filtered_payload.permit(*usage_param_keys).tap do |whitelisted|
          whitelisted_hash_keys.each do |key|
            whitelisted[key] = filtered_payload[key].permit! if filtered_payload[key].present?
          end

          whitelisted[:stats] = filtered_payload[:counts].permit! if filtered_payload[:counts].present?
        end

        UsagePing::ServiceHandler.new.execute(
          usage_data_params.to_h, SOURCE_IP, raw_usage_data.id
        )
      rescue StandardError
      end
      # rubocop:enable Naming/InclusiveLanguage
    end
  end
end
