# frozen_string_literal: true

module Salesforce
  ##
  # All the metrics that we are pushing in the SFDC
  # should be explicitly enumerated in this.
  #
  # This should match closely the Usage_Ping_Data__c
  # object in SFDC
  #
  USAGE_PING_WHITELIST = %i[
    Account_Manager__c
    Account_Owner__c
    Account__c
    Lead__c
    License_Utilization__c
    active_user_count__c
    conv_dev_index__c
    counts__c
    count_auto_devops_disabled__c
    count_auto_devops_enabled__c
    count_boards__c
    count_ci_builds__c
    count_ci_external_pipelines__c
    count_ci_internal_pipelines__c
    count_ci_pipeline_config_auto_devops__c
    count_ci_pipeline_config_repository__c
    count_ci_pipeline_schedules__c
    count_ci_pipelines__c
    count_ci_runners__c
    count_ci_triggers__c
    count_deploy_keys__c
    count_deployments__c
    count_environments__c
    count_geo_nodes__c
    count_groups__c
    count_in_review_folder__c
    count_issues__c
    count_keys__c
    count_labels__c
    count_ldap_group_links__c
    count_ldap_keys__c
    count_ldap_users__c
    count_lfs_objects__c
    count_merge_requests__c
    count_milestones__c
    count_notes__c
    count_pages_domains__c
    count_projects_jira_active__c
    count_projects__c
    count_projects_imported_from_github__c
    count_projects_prometheus_active__c
    count_projects_slack_notif_active__c
    count_projects_slack_slash_active__c
    count_protected_branches__c
    count_releases__c
    count_remote_mirrors__c
    count_service_desk_enabled_projects__c
    count_service_desk_issues__c
    count_services__c
    count_snippets__c
    count_todos__c
    count_uploads__c
    count_web_hooks__c
    created_at__c
    edition__c
    historical_max_users__c
    hostname__c
    installation_type__c
    license_add_ons__c
    license_expires_at__c
    license_md5__c
    license_sha256__c
    license_restricted_count__c
    license_starts_at__c
    license_user_count__c
    license_trial__c
    licensee__c
    mattermost_enabled__c
    recorded_at__c
    source_ip__c
    stats__c
    stats_auto_devops_disabled__c
    stats_auto_devops_enabled__c
    stats_boards__c
    stats_ci_builds__c
    stats_ci_external_pipelines__c
    stats_ci_internal_pipelines__c
    stats_ci_pipeline_config_auto_devops__c
    stats_ci_pipeline_config_repository__c
    stats_ci_pipeline_schedules__c
    stats_ci_pipelines__c
    stats_ci_runners__c
    stats_ci_triggers__c
    stats_deploy_keys__c
    stats_deployments__c
    stats_environments__c
    stats_geo_nodes__c
    stats_groups__c
    stats_in_review_folder__c
    stats_issues__c
    stats_keys__c
    stats_labels__c
    stats_ldap_group_links__c
    stats_ldap_keys__c
    stats_ldap_users__c
    stats_lfs_objects__c
    stats_merge_requests__c
    stats_milestones__c
    stats_notes__c
    stats_pages_domains__c
    stats_projects_jira_active__c
    stats_projects__c
    stats_projects_imported_from_github__c
    stats_projects_prometheus_active__c
    stats_projects_slack_notif_active__c
    stats_projects_slack_slash_active__c
    stats_protected_branches__c
    stats_releases__c
    stats_remote_mirrors__c
    stats_service_desk_enabled_projects__c
    stats_service_desk_issues__c
    stats_services__c
    stats_snippets__c
    stats_todos__c
    stats_uploads__c
    stats_web_hooks__c
    updated_at__c
    usage_data_id__c
    uuid__c
    version__c
  ].freeze

  ##
  # This will cause metrics fields to be renamed when
  # pushed into salesforce.
  #
  METRICS_MAPPINGS = {
    projects_slack_notifications_active: :projects_slack_notif_active,
    projects_slack_active: :projects_slack_notif_active,
    projects_slack_slash_commands_active: :projects_slack_slash_active
  }.freeze
end
