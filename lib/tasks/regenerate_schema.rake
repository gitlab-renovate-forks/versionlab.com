# frozen_string_literal: true

namespace :regenerate_schema do
  desc 'Marks migrations not existing in schema.rb as down'
  task :fix, [:versions] => [:environment] do |task, args|
    versions = args[:versions].split(/\s/).map(&:strip).compact.map(&:to_i)
    version_params = versions.map { |e| "'#{e}'" }.join(',')
    delete_sql = "DELETE FROM schema_migrations WHERE version IN (#{version_params})"

    sanitized_sql = ActiveRecord::Base.sanitize_sql(delete_sql)
    puts sanitized_sql
    ActiveRecord::Base.connection.execute(sanitized_sql)
  end
end
