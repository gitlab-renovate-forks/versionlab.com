#!/bin/bash

export query="SELECT id
,created_at
,instance_boards
,instance_ci_pipelines
,instance_deployments
,instance_environments
,instance_issues
,instance_merge_requests
,instance_milestones
,instance_notes
,instance_projects_prometheus_active
,instance_service_desk_issues
,leader_boards
,leader_ci_pipelines
,leader_deployments
,leader_environments
,leader_issues
,leader_merge_requests
,leader_milestones
,leader_notes
,leader_projects_prometheus_active
,leader_service_desk_issues
,percentage_boards
,percentage_ci_pipelines
,percentage_deployments
,percentage_environments
,percentage_issues
,percentage_merge_requests
,percentage_milestones
,percentage_notes
,percentage_projects_prometheus_active
,percentage_service_desk_issues
,updated_at
,usage_data_id
FROM conversational_development_indices
WHERE updated_at::date = '$date'"
