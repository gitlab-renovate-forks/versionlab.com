# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ApplicationController, type: :controller do
  controller do
    def index
      raise ActionDispatch::Cookies::CookieOverflow
    end
  end

  describe 'handling CookieOverflow errors' do
    let(:user) { create(:user) }

    before do
      sign_in(user)
      routes.draw { get 'index' => 'anonymous#index' }
    end

    it 'resets the session' do
      expect_any_instance_of(controller.class).to receive(:reset_session)

      get :index
    end

    it 'returns json error response' do
      get :index

      expect(response.status).to eq(413)
      expect(JSON.parse(response.body)).to eq({ 'error' => 'Session expired' })
    end
  end
end
