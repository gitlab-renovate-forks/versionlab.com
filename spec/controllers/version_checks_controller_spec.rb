# frozen_string_literal: true

require 'spec_helper'

describe VersionChecksController do
  describe 'GET #check' do
    let(:current_version) { '0.9.0' }

    let(:gitlab_info) do
      Base64.urlsafe_encode64({ version: current_version, type: 'CE' }.to_json)
    end

    context 'when everything is ok' do
      context 'when requesting json' do
        before do
          create(:version, version: '1.1.1')
          create(:version, version: '1.2.1')
          create(:version, version: '1.2.2')
          create(:version, version: '1.3.1', details: '<h1>Testing</h1>')
        end

        shared_examples 'expected response' do |vulnerability_type, severity, critical_vulnerability|
          before do
            create(:version, version: current_version, vulnerability_type: vulnerability_type)
            get :check, params: { gitlab_info: gitlab_info }, as: :json
          end

          it 'returns latest 3 stable versions, latest stable version of minor, latest version number,' \
            'lastest version details, and upgrade severity' do
            latest_stable_version_of_minor = current_version if vulnerability_type == :not_vulnerable

            version_json = JSON.parse(response.body)
            expect(version_json).to eq(
              {
                'latest_stable_versions' => ['1.3.1', '1.2.2', '1.1.1'],
                'latest_stable_version_of_minor' => latest_stable_version_of_minor,
                'latest_version' => '1.3.1',
                'severity' => severity,
                'critical_vulnerability' => critical_vulnerability,
                'details' => '<h1>Testing</h1>'
              }
            )
          end

          it 'returns json content' do
            expect(response).to have_http_status(:ok)
            expect(response.content_type).to eq('application/json; charset=utf-8')
          end
        end

        context 'without any vulnerabilities' do
          include_examples 'expected response', :not_vulnerable, 'warning', false
        end

        context 'with non_critical vulnerabilities' do
          include_examples 'expected response', :non_critical, 'danger', false
        end

        context 'with critical vulnerabilities' do
          include_examples 'expected response', :critical, 'danger', true
        end

        context 'when vulnerable version is outside last 3 minor versions' do
          json_info = {
            'latest_stable_versions' => ['1.3.1', '1.2.2', '1.1.1'],
            'latest_version' => '1.3.1',
            'severity' => 'danger',
            'critical_vulnerability' => true,
            'details' => '<h1>Testing</h1>'
          }

          context 'with an available stable patch' do
            before do
              create(:version, version: current_version, vulnerability_type: :critical)
              create(:version, version: '0.9.1')
              get :check, params: { gitlab_info: gitlab_info }, as: :json
            end

            it 'provides the available stable patch as latest_stable_version_of_minor' do
              version_json = JSON.parse(response.body)
              expect(version_json).to eq(
                {
                  'latest_stable_version_of_minor' => '0.9.1'
                }.merge(json_info)
              )
            end
          end

          context 'with an available patch but it is vulnerable' do
            before do
              create(:version, version: current_version, vulnerability_type: :critical)
              create(:version, version: '0.9.1', vulnerability_type: :critical)
              get :check, params: { gitlab_info: gitlab_info }, as: :json
            end

            it 'does not provide an available stable patch to latest_stable_version_of_minor' do
              version_json = JSON.parse(response.body)
              expect(version_json).to eq(
                {
                  'latest_stable_version_of_minor' => nil
                }.merge(json_info)
              )
            end
          end

          context 'with no available patch' do
            before do
              create(:version, version: current_version, vulnerability_type: :critical)
              get :check, params: { gitlab_info: gitlab_info }, as: :json
            end

            it 'does not provide an available stable patch to latest_stable_version_of_minor' do
              version_json = JSON.parse(response.body)
              expect(version_json).to eq(
                {
                  'latest_stable_version_of_minor' => nil
                }.merge(json_info)
              )
            end
          end
        end
      end

      context 'when requesting png' do
        it 'returns image' do
          get :check, params: { gitlab_info: gitlab_info }, as: :png

          expect(response).to have_http_status(:ok)
          expect(response.content_type).to eq('image/png')
        end
      end

      context 'when requesting svg' do
        it 'returns image' do
          get :check, params: { gitlab_info: gitlab_info }, as: :svg

          expect(response).to have_http_status(:ok)
          expect(response.content_type).to eq('image/svg+xml')
        end
      end
    end

    context 'when hostname is not valid url' do
      before do
        request.env['HTTP_REFERER'] = 'something'
      end

      it 'returns status 200' do
        get :check, params: { gitlab_info: gitlab_info }, as: :png

        expect(response).to have_http_status(:ok)
      end

      it 'returns update_failed image' do
        get :check, params: { gitlab_info: gitlab_info }, as: :png

        expect(response.header['Content-Disposition']).to include('update_failed.png')
      end
    end

    context 'when version seems invalid' do
      let(:gitlab_info) do
        Base64.urlsafe_encode64({ version: 'v11.9.9.v20190904.2', type: 'CE' }.to_json)
      end

      it 'returns status 200' do
        get :check, params: { gitlab_info: gitlab_info }, as: :png

        expect(response).to have_http_status(:ok)
      end
    end

    context 'when version is incorrectly encoded' do
      let(:gitlab_info) { 'eyj2zxbzaw9uijoimtuunc4xlwvrin0=' }

      before do
        get :check, params: { gitlab_info: gitlab_info }, as: :json
      end

      it 'returns status 400' do
        expect(response).to have_http_status(:bad_request)
      end
    end
  end
end
