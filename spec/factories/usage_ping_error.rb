# frozen_string_literal: true

require_relative '../support/usage_data_helpers'

FactoryBot.define do
  factory :usage_ping_error do
    version
    host
    uuid { SecureRandom.uuid }
    elapsed { rand(0.1..10.0).round(2) }
    message { 'Error' }
  end
end
