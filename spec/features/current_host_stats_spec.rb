# frozen_string_literal: true

require 'spec_helper'

describe 'Hosts' do
  describe 'Listing hosts' do
    let!(:current_host_stat) { create(:current_host_stat, url: 'https://gitlab.example.com') }
    let(:user) { create(:user) }

    before do
      login_with(user)
      visit current_host_stats_path
    end

    it { expect(page).to have_content(current_host_stat.url) }
    it { expect(page).to have_content('Last Checked On') }

    context 'when searching' do
      it 'remembers the sorting' do
        click_link 'Licensed User Count'

        click_button 'Search'

        expect(page).to have_link('Licensed User Count', class: 'current desc')
        expect_blank_search
        expect_all_hosts
      end

      it 'remembers the filter button setting on search' do
        click_link 'Starred only'

        click_button 'Search'

        expect_blank_search
        expect_all_hosts_defaults
        expect_starred_hosts
      end

      it 'remembers the filter and sorting' do
        click_link 'Starred only'
        click_link 'Licensed User Count'

        click_button 'Search'

        expect_blank_search
        expect_all_hosts_defaults('Licensed User Count')
        expect_starred_hosts
      end

      context 'when analyzing results' do
        let!(:url_host_1) { create(:current_host_stat, url: 'https://by_host_url.first.com') }
        let!(:url_host_2) { create(:current_host_stat, url: 'https://by_host_url.second.com') }
        let!(:license_host_1) { create(:current_host_stat, url: 'https://by_license.first.com', license_md5: 'abc1') }
        let!(:license_host_2) { create(:current_host_stat, url: 'https://by_license.second.com', license_md5: 'abc1') }
        let!(:license_host_3) { create(:current_host_stat, url: 'https://by_license.third.com', license_md5: 'abc123') }

        context 'when not performing a search' do
          it 'shows all hosts' do
            visit current_url

            expect(page).to have_text 'https://by_', count: 5
            expect(page).to have_text 'https://gitlab.example.com', count: 1
          end
        end

        context 'when performing a search' do
          context 'when using Host URL searching' do
            before do
              select 'Host URL', from: 'search_type'
            end

            it 'has results based on search params' do
              fill_in 'search', with: 'by_host_url'

              click_button 'Search'

              expect(page).to have_link(url_host_1.url)
              expect(page).to have_link(url_host_2.url)
              expect(page).to have_no_content('https://by_license')
            end

            it 'has no results based on search params' do
              fill_in 'search', with: 'bad_search'

              click_button 'Search'

              expect(page).to have_no_content('https://by_')
            end
          end

          context 'when using License MD5 searching' do
            before do
              select 'License MD5', from: 'search_type'
            end

            it 'has results based on search params' do
              fill_in 'search', with: 'abc1'

              click_button 'Search'

              expect(page).to have_link(license_host_1.url)
              expect(page).to have_link(license_host_2.url)
              expect(page).not_to have_link(license_host_3.url)
              expect(page).to have_no_content('https://by_host_url')
            end

            it 'has no results based on search params' do
              fill_in 'search', with: 'bad_search'

              click_button 'Search'

              expect(page).to have_no_content('https://by_')
            end
          end
        end
      end
    end

    describe 'column headers' do
      context 'when searching' do
        it 'remembers the search on sorting column header click' do
          click_button 'Search'

          click_link 'Licensed User Count'

          expect(page).to have_link('Licensed User Count', class: 'current desc')
          expect_blank_search
          expect_all_hosts
        end

        it 'remembers the search and md5 option on column header click' do
          select 'License MD5', from: 'search_type'

          click_button 'Search'

          click_link 'Licensed User Count'

          expect(page).to have_link('Licensed User Count', class: 'current desc')
          expect_blank_search('License MD5')
          expect_all_hosts
        end
      end

      context 'when a filter button is set' do
        context 'without search' do
          it 'remembers the filter button setting on sorting column header click' do
            click_link 'Starred only'

            click_link 'Licensed User Count'

            expect_all_hosts_defaults('Licensed User Count')
            expect_search_defaults
            expect_starred_hosts
          end
        end

        context 'with search' do
          it 'remembers filter and search settings on column header click' do
            click_button 'Search'
            click_link 'Starred only'

            click_link 'Licensed User Count'

            expect_all_hosts_defaults('Licensed User Count')
            expect_blank_search
            expect_starred_hosts
          end
        end
      end
    end

    describe 'filter buttons' do
      context "when 'All hosts' is clicked" do
        it 'retains sorting column and direction' do
          click_link 'Licensed User Count'

          click_link 'Edition: All'

          expect(page).to have_link('Licensed User Count', class: 'current desc')
          expect_search_defaults
          expect_all_hosts
          expect_csv_link(sort: 'user_count')
        end

        it 'applies a default sorting and direction when none are set' do
          click_link 'Edition: All'

          expect(page).to have_link('Host URL', class: 'current desc')
          expect_search_defaults
          expect_all_hosts
          expect_csv_link
        end

        it 'retains the search settings' do
          select 'License MD5', from: 'search_type'
          click_button 'Search'

          click_link 'Edition: All'

          expect(page).to have_link('Host URL', class: 'current desc')
          expect_blank_search('License MD5')
          expect_all_hosts
          expect_csv_link(search_type: 'License MD5', search: '')
        end
      end

      context "when 'Edition: CE' is clicked" do
        it 'retains sorting column and direction' do
          click_link 'Licensed User Count'

          click_link 'Edition: CE'

          expect(page).to have_link('Licensed User Count', class: 'current desc')
          expect_search_defaults
          expect_edition_hosts
          expect_csv_link(edition: 'CE', sort: 'user_count')
        end

        it 'applies a default sorting and direction when none are set' do
          click_link 'Edition: CE'

          expect(page).to have_link('Host URL', class: 'current desc')
          expect_search_defaults
          expect_edition_hosts
          expect_csv_link(edition: 'CE')
        end

        it 'retains the search settings' do
          select 'License MD5', from: 'search_type'
          click_button 'Search'

          click_link 'Edition: CE'

          expect(page).to have_link('Host URL', class: 'current desc')
          expect_blank_search('License MD5')
          expect_edition_hosts
          expect_csv_link(edition: 'CE', search_type: 'License MD5', search: '')
        end
      end

      context "when 'Starred only' is clicked" do
        it 'retains sorting column and direction' do
          click_link 'Licensed User Count'

          click_link 'Starred only'

          expect_search_defaults
          expect_all_hosts_defaults('Licensed User Count')
          expect_starred_hosts
          expect_csv_link(sort: 'user_count', starred: true)
        end

        it 'applies a default sorting and direction when none are set' do
          click_link 'Starred only'

          expect_search_defaults
          expect_all_hosts_defaults
          expect_starred_hosts
          expect_csv_link(starred: true)
        end

        it 'retains the search settings' do
          select 'License MD5', from: 'search_type'
          click_button 'Search'

          click_link 'Starred only'

          expect_blank_search('License MD5')
          expect_all_hosts_defaults
          expect_starred_hosts
          expect_csv_link(search: '', search_type: 'License MD5', starred: true)
        end
      end

      context "when 'Fortune companies only' is clicked" do
        it 'retains sorting column and direction' do
          click_link 'Licensed User Count'

          click_link 'Fortune companies only'

          expect_all_hosts_defaults('Licensed User Count')
          expect_search_defaults
          expect_fortune_ranked_hosts
          expect_csv_link(sort: 'user_count', fortune_rank: true)
        end

        it 'applies a default sorting and direction when none are set' do
          click_link 'Fortune companies only'

          expect_search_defaults
          expect_all_hosts_defaults
          expect_fortune_ranked_hosts
          expect_csv_link(fortune_rank: true)
        end

        it 'retains the search settings' do
          select 'License MD5', from: 'search_type'
          click_button 'Search'

          click_link 'Fortune companies only'

          expect_blank_search('License MD5')
          expect_all_hosts_defaults
          expect_fortune_ranked_hosts
          expect_csv_link(search: '', search_type: 'License MD5', fortune_rank: true)
        end
      end
    end

    describe 'Host details' do
      before do
        click_link current_host_stat.url
      end

      it { expect(page).to have_content(current_host_stat.host.url) }
      it { expect(page).to have_content('Mark this host as starred') }
    end

    describe 'Download CSV' do
      before do
        click_link 'Download CSV'
      end

      it 'includes the correct columns' do
        expect(page).to have_content(
          'host_id,host_link,active_users_count,historical_max_users_count,user_count' \
            ',version,usage_data_recorded_at,edition'
        )
      end

      it { expect(page).not_to have_content('https://gitlab.example.com') }

      it { expect(page).to have_content("http://www.example.com/hosts/#{current_host_stat.host_id}") }
    end
  end

  def expect_blank_search(type = 'Host URL')
    expect(page).to have_field('search', with: '')
    expect(page).to have_field('search_type', with: type)
  end

  def expect_search_defaults(type = 'Host URL')
    expect(page).to have_field('search')
    expect(page).to have_field('search_type', with: type)
  end

  def expect_all_hosts
    expect(page).not_to have_css('#starred', visible: false)
    expect(page).not_to have_css('#fortune_rank', visible: false)
    expect(page).not_to have_css('#edition', visible: false)
    expect(page).to have_button('Edition: All')
  end

  def expect_starred_hosts
    expect(page).to have_css('#starred', visible: false)
    expect(page).not_to have_css('#fortune_rank', visible: false)
    expect(page).not_to have_css('#edition', visible: false)
  end

  def expect_fortune_ranked_hosts
    expect(page).not_to have_css('#starred', visible: false)
    expect(page).to have_css('#fortune_rank', visible: false)
    expect(page).not_to have_css('#edition', visible: false)
  end

  def expect_edition_hosts(edition = 'Edition: CE')
    expect(page).to have_button(edition)
    expect(page).to have_link(edition, class: 'active')
    expect(page).to have_css('#edition', visible: false)
    expect(page).not_to have_css('#starred', visible: false)
    expect(page).not_to have_css('#fortune_rank', visible: false)
  end

  def expect_all_hosts_defaults(column = 'Host URL')
    expect(page).to have_link(column, class: 'current desc')
    expect(page).to have_button('Edition: All')
  end

  def expect_csv_link(params = {})
    default_params = { format: 'csv', sort: 'url', direction: 'desc', search_type: nil, search: nil }
    path_params = default_params.merge(params).compact

    expect(page).to have_link('Download CSV', href: current_host_stats_path(path_params))
  end
end
