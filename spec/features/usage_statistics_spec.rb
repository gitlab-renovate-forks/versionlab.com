# frozen_string_literal: true

require 'spec_helper'

describe 'Usage Statistics' do
  let(:user) { create(:user) }

  describe 'feature usage' do
    it 'displays average value calculated from service pings' do
      create(:usage_data, :recent, stats: { boards: 5 }, uuid: '1')
      create(:usage_data, :recent, stats: { boards: 1 }, uuid: '2')

      login_with(user)
      visit metrics_path

      expect(page).to have_content('boards 3')
    end
  end

  describe 'instance counts' do
    it 'displays the number of licenses and hosts per edition' do
      create(
        :usage_data,
        :recent,
        edition: 'EEP',
        license_md5: 'a',
        uuid: '1'
      )
      create(
        :usage_data,
        :recent,
        edition: 'EEP',
        license_md5: 'a',
        uuid: '2'
      )

      login_with(user)
      visit metrics_path

      expect(page).to have_content(
        'EEP licenses: 1 Unique installations: 2 (avg: 2.00)'
      )
    end
  end
end
