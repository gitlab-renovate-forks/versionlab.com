# frozen_string_literal: true

require 'spec_helper'

describe VersionHelper do
  describe '#vulnerability_type_options_for_select' do
    it 'returns an array of arrays that contain enum data' do
      expected_data = [
        %w[No not_vulnerable],
        ['Non Critical', 'non_critical'],
        %w[Critical critical]
      ]

      expect(helper.vulnerability_type_options_for_select).to eq(expected_data)
    end
  end
end
