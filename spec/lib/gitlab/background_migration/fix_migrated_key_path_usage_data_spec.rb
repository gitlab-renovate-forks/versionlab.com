# frozen_string_literal: true

require 'spec_helper'

describe Gitlab::BackgroundMigration::FixMigratedKeyPathUsageData, :migration do
  subject(:migration) { described_class.new }

  describe "#perform" do
    let(:default_payload) do
      {
        counts: {
          work_item_parent_links: { epic_parent: 5 },
          issues: 3
        },
        uuid: 'abcd',
        recorded_at: recorded_at.to_s
      }
    end

    let(:default_raw_payload) { default_payload.merge(raw_attribute: 13) }
    let(:recorded_at) { Date.yesterday }
    let(:id_range) { [RawUsageData.first.id, RawUsageData.last.id + 1] }
    let(:created_at) { '2024-09-14' }
    let!(:raw_usage_data1) do
      create(:raw_usage_data, payload: default_payload, raw_usage_data_payload: default_raw_payload, created_at: created_at)
    end

    let!(:raw_usage_data2) do
      create(:raw_usage_data, payload: default_payload.merge(metric: 18), raw_usage_data_payload: default_raw_payload, created_at: created_at)
    end

    before do
      # unaffected payload
      create(:raw_usage_data, payload: default_payload.except(:counts), raw_usage_data_payload: default_raw_payload, created_at: created_at)
      # outside the time range
      create(:raw_usage_data, payload: default_payload, raw_usage_data_payload: default_raw_payload, created_at: '2024-08-01')
      create(:raw_usage_data, payload: default_payload, raw_usage_data_payload: default_raw_payload, created_at: '2024-09-23')
    end

    it "correctly creates raw_usage_data and usage_data entries" do
      expect { migration.perform(*id_range) }.to change { [RawUsageData.count, UsageData.count] }.from([5, 0]).to([7, 2])
      expect(UsageData.last.stats['work_item_parent_links_epic_parent']).to eq(5)
      RawUsageData.last(2).each do |new_raw_usage_data|
        expect(new_raw_usage_data.uuid).to eq('abcd')
        expect(new_raw_usage_data.recorded_at).to eq(recorded_at)
        expect(new_raw_usage_data.payload.except('metric')).to eq(default_payload.deep_stringify_keys)
        expect(new_raw_usage_data.raw_usage_data_payload).to eq(default_raw_payload.deep_stringify_keys)
      end
    end

    it "ignores error entries" do
      allow(ActionController::Parameters).to receive(:new).and_call_original
      expect(ActionController::Parameters).to receive(:new).with(default_payload.deep_stringify_keys)
        .and_raise(ArgumentError.new)

      expect { migration.perform(*id_range) }.to change { [RawUsageData.count, UsageData.count] }.from([5, 0]).to([6, 1])
    end
  end
end
