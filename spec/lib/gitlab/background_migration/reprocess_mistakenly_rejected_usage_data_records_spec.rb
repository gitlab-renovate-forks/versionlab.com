# frozen_string_literal: true

require 'spec_helper'

describe Gitlab::BackgroundMigration::ReprocessMistakenlyRejectedUsageDataRecords, :migration do
  subject(:migration) { described_class.new }

  describe "#perform" do
    let(:default_payload) do
      {
        counts: {
          work_item_parent_links: { epic_parent: 5 },
        },
        version: '17.6',
        uuid: 'abcd',
        recorded_at: created_at
      }
    end

    let(:created_at) { '2024-12-01' }
    let(:id_range) { [RawUsageData.first.id, RawUsageData.last.id + 1] }
    let(:default_raw_payload) { default_payload.merge(raw_attribute: 13) }

    before do
      # affected version
      create(:raw_usage_data, payload: default_payload, raw_usage_data_payload: default_raw_payload, created_at: '2024-11-22')
      create(:raw_usage_data, payload: default_payload, raw_usage_data_payload: default_raw_payload, created_at: '2024-12-10')
      # not affected version
      create(:raw_usage_data, payload: default_payload.merge(version: '17.5'), raw_usage_data_payload: default_raw_payload,
       created_at: created_at)
      # outside the time range
      create(:raw_usage_data, payload: default_payload, raw_usage_data_payload: default_raw_payload, created_at: '2024-11-01')
      create(:raw_usage_data, payload: default_payload, raw_usage_data_payload: default_raw_payload, created_at: '2024-12-15')
    end

    it "correctly creates usage_data entries" do
      expect { migration.perform(*id_range) }.to change { UsageData.count }.from(0).to(2)

      expect(UsageData.last.stats['work_item_parent_links_epic_parent']).to eq(5)
    end

    it "ignores error entries" do
      allow(ActionController::Parameters).to receive(:new).and_call_original
      expect(ActionController::Parameters).to receive(:new).with(default_payload.deep_stringify_keys)
                                                           .and_raise(ArgumentError.new)

      expect { migration.perform(*id_range) }.to change { UsageData.count }.from(0).to(1)
    end
  end
end
