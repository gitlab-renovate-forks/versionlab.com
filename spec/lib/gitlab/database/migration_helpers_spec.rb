# frozen_string_literal: true

require 'spec_helper'

describe Gitlab::Database::MigrationHelpers do
  let(:model) do
    ActiveRecord::Migration.new.extend(described_class)
  end

  before do
    allow(model).to receive(:puts)
  end

  shared_examples 'skips validation' do |validation_option|
    it 'skips validation' do
      expect(model).not_to receive(:disable_statement_timeout)
      expect(model).to receive(:execute).with(/ADD CONSTRAINT/)
      expect(model).not_to receive(:execute).with(/VALIDATE CONSTRAINT/)

      model.add_concurrent_foreign_key(*args, **options.merge(validation_option))
    end
  end

  shared_examples 'performs validation' do |validation_option|
    it 'performs validation' do
      expect(model).to receive(:disable_statement_timeout).and_call_original
      expect(model).to receive(:execute).with(/statement_timeout/)
      expect(model).to receive(:execute).ordered.with(/NOT VALID/)
      expect(model).to receive(:execute).ordered.with(/VALIDATE CONSTRAINT/)
      expect(model).to receive(:execute).ordered.with(/RESET ALL/)

      model.add_concurrent_foreign_key(*args, **options.merge(validation_option))
    end
  end

  describe '#add_concurrent_index' do
    context 'when outside a transaction' do
      before do
        allow(model).to receive(:transaction_open?).and_return(false)
        allow(model).to receive(:disable_statement_timeout).and_call_original
      end

      it 'creates the index concurrently' do
        expect(model).to receive(:add_index)
                           .with(:users, :foo, algorithm: :concurrently)

        model.add_concurrent_index(:users, :foo)
      end

      it 'creates unique index concurrently' do
        expect(model).to receive(:add_index)
                           .with(:users, :foo, { algorithm: :concurrently, unique: true })

        model.add_concurrent_index(:users, :foo, unique: true)
      end

      it 'does nothing if the index exists already' do
        expect(model).to receive(:index_exists?)
                           .with(:users, :foo, { algorithm: :concurrently, unique: true }).and_return(true)
        expect(model).not_to receive(:add_index)

        model.add_concurrent_index(:users, :foo, unique: true)
      end
    end

    context 'when inside a transaction' do
      it 'raises RuntimeError' do
        expect(model).to receive(:transaction_open?).and_return(true)

        expect { model.add_concurrent_index(:users, :foo) }
          .to raise_error(RuntimeError)
      end
    end
  end

  describe '#remove_concurrent_index' do
    context 'outside a transaction' do
      before do
        allow(model).to receive(:transaction_open?).and_return(false)
        allow(model).to receive(:index_exists?).and_return(true)
        allow(model).to receive(:disable_statement_timeout).and_call_original
      end

      describe 'by column name' do
        it 'removes the index concurrently' do
          expect(model).to receive(:remove_index)
                             .with(:users, { algorithm: :concurrently, column: :foo })

          model.remove_concurrent_index(:users, :foo)
        end

        it 'does nothing if the index does not exist' do
          expect(model).to receive(:index_exists?)
                             .with(:users, :foo, { algorithm: :concurrently, unique: true }).and_return(false)
          expect(model).not_to receive(:remove_index)

          model.remove_concurrent_index(:users, :foo, unique: true)
        end
      end

      describe 'by index name' do
        before do
          allow(model).to receive(:index_exists_by_name?).with(:users, "index_x_by_y").and_return(true)
        end

        it 'removes the index concurrently by index name' do
          expect(model).to receive(:remove_index)
                             .with(:users, { algorithm: :concurrently, name: "index_x_by_y" })

          model.remove_concurrent_index_by_name(:users, "index_x_by_y")
        end

        it 'does nothing if the index does not exist' do
          expect(model).to receive(:index_exists_by_name?).with(:users, "index_x_by_y").and_return(false)
          expect(model).not_to receive(:remove_index)

          model.remove_concurrent_index_by_name(:users, "index_x_by_y")
        end
      end
    end

    context 'inside a transaction' do
      it 'raises RuntimeError' do
        expect(model).to receive(:transaction_open?).and_return(true)

        expect { model.remove_concurrent_index(:users, :foo) }
          .to raise_error(RuntimeError)
      end
    end
  end

  describe '#disable_statement_timeout' do
    # this specs runs without an enclosing transaction (:delete truncation method for db_cleaner)
    context 'with real environment', :delete do
      before do
        model.execute("SET statement_timeout TO '20000'")
      end

      after do
        model.execute('RESET ALL')
      end

      context 'when passing a block' do
        it 'disables statement timeouts on session level and executes the block' do
          expect(model).to receive(:execute).with('SET statement_timeout TO 0')
          expect(model).to receive(:execute).with('RESET ALL').at_least(:once)

          expect { |block| model.disable_statement_timeout(&block) }.to yield_control
        end

        # this specs runs without an enclosing transaction (:delete truncation method for db_cleaner)
        context 'with real environment', :delete do
          before do
            model.execute("SET statement_timeout TO '20000'")
          end

          after do
            model.execute('RESET ALL')
          end

          it 'defines statement to 0 for any code run inside the block' do
            expect(model.execute('SHOW statement_timeout').first['statement_timeout']).to eq('20s')

            model.disable_statement_timeout do
              model.connection.transaction do
                expect(model.execute('SHOW statement_timeout').first['statement_timeout']).to eq('0')
              end

              expect(model.execute('SHOW statement_timeout').first['statement_timeout']).to eq('0')
            end
          end
        end
      end
    end
  end

  describe '#update_column_in_batches' do
    context 'when running outside of a transaction' do
      before do
        expect(model).to receive(:transaction_open?).and_return(false)

        create_list(:host, 5)
        create_list(:user, 5)
      end

      it 'updates all the rows in a table' do
        model.update_column_in_batches(:users, :private_token, 'foo')

        expect(User.where(private_token: 'foo').count).to eq(5)
      end

      it 'updates boolean values correctly' do
        model.update_column_in_batches(:hosts, :star, true)

        expect(Host.where(star: true).count).to eq(5)
      end

      context 'when a block is supplied' do
        it 'yields an Arel table and query object to the supplied block' do
          first_id = Host.first.id

          model.update_column_in_batches(:hosts, :star, true) do |t, query|
            query.where(t[:id].eq(first_id))
          end

          expect(Host.where(star: true).count).to eq(1)
        end
      end

      context 'when the value is Arel.sql (Arel::Nodes::SqlLiteral)' do
        it 'updates the value as a SQL expression' do
          model.update_column_in_batches(:users, :sign_in_count, Arel.sql('1+1'))

          expect(User.sum(:sign_in_count)).to eq(2 * User.count)
        end
      end
    end

    context 'when running inside the transaction' do
      it 'raises RuntimeError' do
        expect(model).to receive(:transaction_open?).and_return(true)

        expect do
          model.update_column_in_batches(:users, :sign_in_count, Arel.sql('1+1'))
        end.to raise_error(RuntimeError)
      end
    end
  end

  describe '#column_for' do
    it 'returns a column object for an existing column' do
      column = model.column_for(:users, :id)

      expect(column.name).to eq('id')
    end

    it 'raises an error when a column does not exist' do
      error_message = /Could not find column "kittens" on table "users"/
      expect { model.column_for(:users, :kittens) }.to raise_error(error_message)
    end
  end

  describe '#queue_background_migration_jobs_by_range_at_intervals' do
    context 'when the model has an ID column' do
      let!(:id1) { create(:user).id }
      let!(:id2) { create(:user).id }
      let!(:id3) { create(:user).id }

      around do |example|
        freeze_time { example.run }
      end

      before do
        User.class_eval do
          include EachBatch
        end
      end

      after do
        BackgroundMigrationWorker.clear
      end

      context 'with batch_size option' do
        it 'queues jobs correctly' do
          model.queue_background_migration_jobs_by_range_at_intervals(User, 'FooJob', 10.minutes, batch_size: 2)

          test_jobs = BackgroundMigrationWorker.jobs.select { |x| x['args'].include?('FooJob') }
          expect(test_jobs[0]['args']).to eq(['FooJob', [id1, id2]])
          expect(test_jobs[0]['at']).to eq(10.minutes.from_now.to_f)
          expect(test_jobs[1]['args']).to eq(['FooJob', [id3, id3]])
          expect(test_jobs[1]['at']).to eq(20.minutes.from_now.to_f)
        end
      end

      context 'without batch_size option' do
        it 'queues jobs correctly' do
          model.queue_background_migration_jobs_by_range_at_intervals(User, 'FooJob', 10.minutes)

          test_jobs = BackgroundMigrationWorker.jobs.select { |x| x['args'].include?('FooJob') }
          expect(test_jobs[0]['args']).to eq(['FooJob', [id1, id3]])
          expect(test_jobs[0]['at']).to eq(10.minutes.from_now.to_f)
        end
      end

      context 'with other_arguments option' do
        it 'queues jobs correctly' do
          model.queue_background_migration_jobs_by_range_at_intervals(User, 'FooJob', 10.minutes, other_arguments: [1, 2])

          test_jobs = BackgroundMigrationWorker.jobs.select { |x| x['args'].include?('FooJob') }
          expect(test_jobs[0]['args']).to eq(['FooJob', [id1, id3, 1, 2]])
          expect(test_jobs[0]['at']).to eq(10.minutes.from_now.to_f)
        end
      end
    end

    context "when the model doesn't have an ID column" do
      before do
        class ModelWithNoIdColumn
          def self.column_names
            []
          end
        end
      end

      it 'raises error (for now)' do
        expect do
          model.queue_background_migration_jobs_by_range_at_intervals(ModelWithNoIdColumn, 'FooJob', 10.seconds)
        end.to raise_error(StandardError, /does not have an ID/)
      end
    end
  end

  describe '#index_exists_by_name?' do
    it 'returns true if an index exists' do
      ActiveRecord::Base.connection.execute(
        'CREATE INDEX test_index_for_index_exists ON users (email);'
      )

      expect(model.index_exists_by_name?(:users, 'test_index_for_index_exists'))
        .to be_truthy
    end

    it 'returns false if the index does not exist' do
      expect(model.index_exists_by_name?(:users, 'this_does_not_exist'))
        .to be_falsy
    end

    context 'when an index with a function exists' do
      before do
        ActiveRecord::Base.connection.execute(
          'CREATE INDEX test_index ON users (LOWER(email));'
        )
      end

      after do
        'DROP INDEX IF EXISTS test_index;'
      end

      it 'returns true if an index exists' do
        expect(model.index_exists_by_name?(:users, 'test_index'))
          .to be_truthy
      end
    end
  end

  describe '#add_concurrent_foreign_key' do
    before do
      allow(model).to receive(:foreign_key_exists?).and_return(false)
    end

    context 'inside a transaction' do
      it 'raises an error' do
        expect(model).to receive(:transaction_open?).and_return(true)

        expect do
          model.add_concurrent_foreign_key(:usage_data, :raw_usage_data, column: :raw_usage_data_id)
        end.to raise_error(RuntimeError)
      end
    end

    context 'outside a transaction' do
      before do
        allow(model).to receive(:transaction_open?).and_return(false)
      end

      context 'ON DELETE statements' do
        context 'on_delete: :nullify' do
          it 'appends ON DELETE SET NULL statement' do
            expect(model).to receive(:with_lock_retries).and_call_original
            expect(model).to receive(:disable_statement_timeout).and_call_original
            expect(model).to receive(:execute).with(/statement_timeout/)
            expect(model).to receive(:execute).ordered.with(/VALIDATE CONSTRAINT/)
            expect(model).to receive(:execute).ordered.with(/RESET ALL/)

            expect(model).to receive(:execute).with(/ON DELETE SET NULL/)

            model.add_concurrent_foreign_key(:usage_data, :raw_usage_data,
                                             column: :raw_usage_data_id,
                                             on_delete: :nullify)
          end
        end

        context 'on_delete: :cascade' do
          it 'appends ON DELETE CASCADE statement' do
            expect(model).to receive(:with_lock_retries).and_call_original
            expect(model).to receive(:disable_statement_timeout).and_call_original
            expect(model).to receive(:execute).with(/statement_timeout/)
            expect(model).to receive(:execute).ordered.with(/VALIDATE CONSTRAINT/)
            expect(model).to receive(:execute).ordered.with(/RESET ALL/)

            expect(model).to receive(:execute).with(/ON DELETE CASCADE/)

            model.add_concurrent_foreign_key(:usage_data, :raw_usage_data,
                                             column: :raw_usage_data_id,
                                             on_delete: :cascade)
          end
        end

        context 'on_delete: nil' do
          it 'appends no ON DELETE statement' do
            expect(model).to receive(:with_lock_retries).and_call_original
            expect(model).to receive(:disable_statement_timeout).and_call_original
            expect(model).to receive(:execute).with(/statement_timeout/)
            expect(model).to receive(:execute).ordered.with(/VALIDATE CONSTRAINT/)
            expect(model).to receive(:execute).ordered.with(/RESET ALL/)

            expect(model).not_to receive(:execute).with(/ON DELETE/)

            model.add_concurrent_foreign_key(:usage_data, :raw_usage_data,
                                             column: :raw_usage_data_id,
                                             on_delete: nil)
          end
        end
      end

      context 'when no custom key name is supplied' do
        it 'creates a concurrent foreign key and validates it' do
          expect(model).to receive(:with_lock_retries).and_call_original
          expect(model).to receive(:disable_statement_timeout).and_call_original
          expect(model).to receive(:execute).with(/statement_timeout/)
          expect(model).to receive(:execute).ordered.with(/NOT VALID/)
          expect(model).to receive(:execute).ordered.with(/VALIDATE CONSTRAINT/)
          expect(model).to receive(:execute).ordered.with(/RESET ALL/)

          model.add_concurrent_foreign_key(:usage_data, :raw_usage_data, column: :raw_usage_data_id)
        end

        it 'does not create a foreign key if it exists already' do
          name = model.concurrent_foreign_key_name(:usage_data, :raw_usage_data_id)
          expect(model).to receive(:foreign_key_exists?).with(:usage_data, :raw_usage_data,
                                                              column: :raw_usage_data_id,
                                                              on_delete: :cascade,
                                                              name: name).and_return(true)

          expect(model).not_to receive(:execute).with(/ADD CONSTRAINT/)
          expect(model).to receive(:execute).with(/VALIDATE CONSTRAINT/)

          model.add_concurrent_foreign_key(:usage_data, :raw_usage_data, column: :raw_usage_data_id)
        end
      end

      context 'when a custom key name is supplied' do
        context 'for creating a new foreign key for a column that does not presently exist' do
          it 'creates a new foreign key' do
            expect(model).to receive(:with_lock_retries).and_call_original
            expect(model).to receive(:disable_statement_timeout).and_call_original
            expect(model).to receive(:execute).with(/statement_timeout/)
            expect(model).to receive(:execute).ordered.with(/NOT VALID/)
            expect(model).to receive(:execute).ordered.with(/VALIDATE CONSTRAINT.+foo/)
            expect(model).to receive(:execute).ordered.with(/RESET ALL/)

            model.add_concurrent_foreign_key(:usage_data, :raw_usage_data, column: :raw_usage_data_id, name: :foo)
          end
        end

        context 'for creating a duplicate foreign key for a column that presently exists' do
          context 'when the supplied key name is the same as the existing foreign key name' do
            it 'does not create a new foreign key' do
              expect(model).to receive(:foreign_key_exists?).with(:usage_data, :raw_usage_data,
                                                                  name: :foo,
                                                                  on_delete: :cascade,
                                                                  column: :raw_usage_data_id).and_return(true)

              expect(model).not_to receive(:execute).with(/ADD CONSTRAINT/)
              expect(model).to receive(:execute).with(/VALIDATE CONSTRAINT/)

              model.add_concurrent_foreign_key(:usage_data, :raw_usage_data, column: :raw_usage_data_id, name: :foo)
            end
          end

          context 'when the supplied key name is different from the existing foreign key name' do
            it 'creates a new foreign key' do
              expect(model).to receive(:with_lock_retries).and_call_original
              expect(model).to receive(:disable_statement_timeout).and_call_original
              expect(model).to receive(:execute).with(/statement_timeout/)
              expect(model).to receive(:execute).ordered.with(/NOT VALID/)
              expect(model).to receive(:execute).ordered.with(/VALIDATE CONSTRAINT.+bar/)
              expect(model).to receive(:execute).ordered.with(/RESET ALL/)

              model.add_concurrent_foreign_key(:usage_data, :raw_usage_data, column: :raw_usage_data_id, name: :bar)
            end
          end
        end
      end

      describe 'validate option' do
        let(:args) { [:usage_data, :raw_usage_data] }
        let(:options) { { column: :raw_usage_data_id, on_delete: nil } }

        context 'when validate is supplied with a falsey value' do
          it_behaves_like 'skips validation', validate: false
          it_behaves_like 'skips validation', validate: nil
        end

        context 'when validate is supplied with a truthy value' do
          it_behaves_like 'performs validation', validate: true
          it_behaves_like 'performs validation', validate: :whatever
        end

        context 'when validate is not supplied' do
          it_behaves_like 'performs validation', {}
        end
      end
    end
  end

  describe '#concurrent_foreign_key_name' do
    it 'returns the name for a foreign key' do
      name = model.concurrent_foreign_key_name(:this_is_a_very_long_table_name,
                                               :with_a_very_long_column_name)

      expect(name).to be_an_instance_of(String)
      expect(name.length).to eq(13)
    end
  end

  describe '#foreign_key_exists?' do
    before do
      key = ActiveRecord::ConnectionAdapters::ForeignKeyDefinition.new(:usage_data, :raw_usage_data, { column: :non_standard_id, name: :fk_usage_data_raw_usage_data_non_standard_id, on_delete: :cascade })
      allow(model).to receive(:foreign_keys).with(:usage_data).and_return([key])
    end

    shared_examples_for 'foreign key checks' do
      it 'finds existing foreign keys by column' do
        expect(model.foreign_key_exists?(:usage_data, target_table, column: :non_standard_id)).to be_truthy
      end

      it 'finds existing foreign keys by name' do
        expect(model.foreign_key_exists?(:usage_data, target_table, name: :fk_usage_data_raw_usage_data_non_standard_id)).to be_truthy
      end

      it 'finds existing foreign_keys by name and column' do
        expect(model.foreign_key_exists?(:usage_data, target_table, name: :fk_usage_data_raw_usage_data_non_standard_id, column: :non_standard_id)).to be_truthy
      end

      it 'finds existing foreign_keys by name, column and on_delete' do
        expect(model.foreign_key_exists?(:usage_data, target_table, name: :fk_usage_data_raw_usage_data_non_standard_id, column: :non_standard_id, on_delete: :cascade)).to be_truthy
      end

      it 'finds existing foreign keys by target table only' do
        expect(model.foreign_key_exists?(:usage_data, target_table)).to be_truthy
      end

      it 'compares by column name if given' do
        expect(model.foreign_key_exists?(:usage_data, target_table, column: :user_id)).to be_falsey
      end

      it 'compares by foreign key name if given' do
        expect(model.foreign_key_exists?(:usage_data, target_table, name: :non_existent_foreign_key_name)).to be_falsey
      end

      it 'compares by foreign key name and column if given' do
        expect(model.foreign_key_exists?(:usage_data, target_table, name: :non_existent_foreign_key_name, column: :non_standard_id)).to be_falsey
      end

      it 'compares by foreign key name, column and on_delete if given' do
        expect(model.foreign_key_exists?(:usage_data, target_table, name: :fk_usage_data_raw_usage_data_non_standard_id, column: :non_standard_id, on_delete: :nullify)).to be_falsey
      end
    end

    context 'without specifying a target table' do
      let(:target_table) { nil }

      it_behaves_like 'foreign key checks'
    end

    context 'specifying a target table' do
      let(:target_table) { :raw_usage_data }

      it_behaves_like 'foreign key checks'
    end

    it 'compares by target table if no column given' do
      expect(model.foreign_key_exists?(:usage_data, :other_table)).to be_falsey
    end
  end

  describe '#rename_column_using_background_migration' do
    let!(:usage_data) { create(:usage_data, :with_stats) }

    it 'renames a column using a background migration' do
      expect(model)
        .to receive(:add_column)
              .with(
                'usage_data',
                :old_stats,
                :json,
                limit: anything,
                precision: anything,
                scale: anything
              )

      expect(model)
        .to receive(:change_column_default)
              .with(
                'usage_data',
                :old_stats,
                "{}"
              )

      expect(model)
        .to receive(:install_rename_triggers)
              .with('usage_data', :stats, :old_stats)

      expect(BackgroundMigrationWorker)
        .to receive(:perform_in)
              .ordered
              .with(
                10.minutes,
                'CopyColumn',
                ['usage_data', :stats, :old_stats, usage_data.id, usage_data.id]
              )

      expect(BackgroundMigrationWorker)
        .to receive(:perform_in)
              .ordered
              .with(
                1.hour + 10.minutes,
                'CleanupConcurrentRename',
                ['usage_data', :stats, :old_stats]
              )

      expect(Gitlab::BackgroundMigration)
        .to receive(:steal)
              .ordered
              .with('CopyColumn')

      expect(Gitlab::BackgroundMigration)
        .to receive(:steal)
              .ordered
              .with('CleanupConcurrentRename')

      model.rename_column_using_background_migration(
        'usage_data',
        :stats,
        :old_stats
      )
    end
  end

  describe '#perform_background_migration_inline?' do
    it 'returns true in a test environment' do
      stub_rails_env('test')

      expect(model.perform_background_migration_inline?).to eq(true)
    end

    it 'returns true in a development environment' do
      stub_rails_env('development')

      expect(model.perform_background_migration_inline?).to eq(true)
    end

    it 'returns false in a production environment' do
      stub_rails_env('production')

      expect(model.perform_background_migration_inline?).to eq(false)
    end
  end

  describe '#rename_trigger_name' do
    it 'returns a String' do
      expect(model.rename_trigger_name(:users, :foo, :bar))
        .to match(/trigger_.{12}/)
    end
  end

  describe '#install_rename_triggers_for_postgresql' do
    it 'installs the triggers for PostgreSQL' do
      expect(model).to receive(:execute)
                         .with(/CREATE OR REPLACE FUNCTION foo()/m)

      expect(model).to receive(:execute)
                         .with(/DROP TRIGGER IF EXISTS foo/m)

      expect(model).to receive(:execute)
                         .with(/CREATE TRIGGER foo/m)

      model.install_rename_triggers_for_postgresql('foo', :users, :old, :new)
    end

    it 'does not fail if trigger already exists' do
      model.install_rename_triggers_for_postgresql('foo', :users, :old, :new)
      model.install_rename_triggers_for_postgresql('foo', :users, :old, :new)
    end
  end

  describe '#cleanup_concurrent_column_rename' do
    it 'cleans up the renaming procedure' do
      expect(model).to receive(:remove_rename_triggers_for_postgresql)
                         .with(:users, /trigger_.{12}/)

      expect(model).to receive(:remove_column).with(:users, :old)

      model.cleanup_concurrent_column_rename(:users, :old, :new)
    end
  end

  describe '#remove_rename_triggers_for_postgresql' do
    it 'removes the function and trigger' do
      expect(model).to receive(:execute).with('DROP TRIGGER IF EXISTS foo ON bar')
      expect(model).to receive(:execute).with('DROP FUNCTION IF EXISTS foo()')

      model.remove_rename_triggers_for_postgresql('bar', 'foo')
    end
  end
end
