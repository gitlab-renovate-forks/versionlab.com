# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'sortable' do
  describe '.order_by' do
    it 'is a custom order by operation' do
      allow(described_class).to receive(:order_by_version)

      described_class.order_by('version', 'asc')

      expect(described_class).to have_received(:order_by_version).once
    end

    context 'with valid column' do
      before do
        allow(described_class).to receive(:order)
        allow(described_class).to receive(:column_names).and_return(%w[id valid_column])
      end

      it 'is a valid order by column operation' do
        described_class.order_by('valid_column', 'asc')

        expect(described_class).to have_received(:order).once
      end

      it 'is an invalid order by column operation' do
        expect { described_class.order_by('invalid_column', 'asc') }
            .to raise_error(Sortable::InvalidSortColumn).with_message(/not permitted/)
      end

      it 'is an invalid order by direction operation' do
        expect { described_class.order_by('valid_column', 'bad_direction') }
            .to raise_error(Sortable::InvalidSortDirection).with_message(/not permitted/)
      end
    end
  end
end
