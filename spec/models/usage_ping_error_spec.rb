# frozen_string_literal: true

require 'spec_helper'

RSpec.describe UsagePingError, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:uuid) }

    context "when message is null" do
      let(:usage_ping_error) { build(:usage_ping_error, message: nil) }

      it "record should be valid" do
        expect(usage_ping_error).to be_valid
      end
    end

    context "when message is present" do
      let(:usage_ping_error) { build(:usage_ping_error, message: 'test') }

      it "record should be valid" do
        expect(usage_ping_error).to be_valid
      end
    end
  end
end
