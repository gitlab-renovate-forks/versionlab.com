# frozen_string_literal: true

require 'spec_helper'

RSpec.describe UsagePingMetadatum, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:metrics) }

    it 'passes for a valid record' do
      expect(described_class.new(uuid: 'a', metrics: [{ a: 1 }])).to be_valid
    end
  end
end
