# frozen_string_literal: true

require 'spec_helper'

describe "VersionChecks" do
  describe "GET /check.png" do
    # backward compatibility
    it 'responds 200 for valid request with HTTP REFERER' do
      check(:png, headers: { 'HTTP_REFERER' => 'https://gitlab.com/admin' })

      expect(response).to be_ok
      expect(response.content_type).to eq 'image/png'
    end

    it 'responds 200 for valid request' do
      check(:png)

      expect(response).to be_ok
      expect(response.content_type).to eq 'image/png'
    end
  end

  describe "GET /check.svg" do
    # backward compatibility
    it 'responds 200 for valid request with HTTP REFERER' do
      check(:svg, headers: { 'HTTP_REFERER' => 'https://gitlab.com/admin' })

      expect(response).to be_ok
      expect(response.content_type).to eq 'image/svg+xml'
    end

    it 'responds 200 for valid request' do
      check(:svg)

      expect(response).to be_ok
      expect(response.content_type).to eq 'image/svg+xml'
    end
  end

  def check(format = :png, info: encrypted_string, headers: {})
    get "/check.#{format}?gitlab_info=#{info}", params: {}, headers: headers
  end

  def encrypted_string
    string = { "version" => "7.8.0.pre" }.to_json
    Base64.urlsafe_encode64(string)
  end
end
