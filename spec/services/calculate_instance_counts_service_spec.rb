# frozen_string_literal: true

require 'spec_helper'

describe CalculateInstanceCountsService do
  let(:service) { described_class.new }

  it 'returns data for all editions' do
    create(
      :usage_data,
      :recent,
      edition: 'EE',
      license_md5: 'a'
    )

    result = service.execute

    expect(result['CE'].instances).to eq 0
    expect(result['CE'].licenses).to eq 0
    expect(result['CE'].ratio).to eq 0

    expect(result['EE'].instances).to eq 1
    expect(result['EE'].licenses).to eq 1
    expect(result['EE'].ratio).to eq 1

    expect(result['EES'].instances).to eq 0
    expect(result['EES'].licenses).to eq 0
    expect(result['EES'].ratio).to eq 0

    expect(result['EEP'].instances).to eq 0
    expect(result['EEP'].licenses).to eq 0
    expect(result['EEP'].ratio).to eq 0
  end

  describe 'time window' do
    it 'ignores instances that were not active in the last 30 days' do
      create(:usage_data, edition: 'CE', recorded_at: 31.days.ago)

      expect(service.execute['CE'].instances).to eq 0
    end
  end

  describe 'multiple pings from the same instance' do
    it 'counts them as one instance' do
      create(
        :usage_data,
        :recent,
        edition: 'EEP',
        license_md5: 'a',
        uuid: '1'
      )
      create(
        :usage_data,
        :recent,
        edition: 'EEP',
        license_md5: 'a',
        uuid: '1'
      )

      result = service.execute['EEP']

      expect(result.licenses).to eq 1
      expect(result.instances).to eq 1
    end
  end

  describe 'multiple pings from the same license' do
    it 'counts instances separately' do
      create(
        :usage_data,
        :recent,
        edition: 'EES',
        license_md5: 'a',
        uuid: '1'
      )
      create(
        :usage_data,
        :recent,
        edition: 'EES',
        license_md5: 'a',
        uuid: '2'
      )

      result = service.execute['EES']

      expect(result.licenses).to eq 1
      expect(result.instances).to eq 2
      expect(result.ratio).to eq 2
    end
  end

  describe 'ratio' do
    it 'calculates the ratio between instances and licenses' do
      create(
        :usage_data,
        :recent,
        edition: 'EE',
        license_md5: 'a',
        uuid: '1'
      )
      create(
        :usage_data,
        :recent,
        edition: 'EE',
        license_md5: 'a',
        uuid: '2'
      )
      create(
        :usage_data,
        :recent,
        edition: 'EE',
        license_md5: 'b',
        uuid: '3'
      )

      result = service.execute['EE']

      expect(result.licenses).to eq 2
      expect(result.instances).to eq 3
      expect(result.ratio).to eq 1.5
    end
  end
end
