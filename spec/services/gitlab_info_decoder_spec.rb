# frozen_string_literal: true

require 'spec_helper'

describe GitlabInfoDecoder do
  let(:source_gitlab_info) { { version: '9.4.0', type: 'CE' } }
  let(:gitlab_info) { Base64.urlsafe_encode64(source_gitlab_info.to_json) }

  describe '.decode' do
    subject { described_class.decode(gitlab_info) }

    context 'when gitlab_info is valid' do
      it 'decodes the data' do
        expect(subject).to eq(source_gitlab_info.stringify_keys)
      end
    end

    context 'when the gitlab_info is not a valid JSON string' do
      let(:gitlab_info) { Base64.urlsafe_encode64('source_gitlab_info') }

      it 'returns nil' do
        expect(subject).to be_nil
      end

      it 'logs the error' do
        expect(Rails.logger).to receive(:warn).with('GitlabInfoDecoder: Gitlab info not decoded')

        subject
      end
    end
  end
end
