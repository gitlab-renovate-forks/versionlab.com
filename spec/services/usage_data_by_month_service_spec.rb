# frozen_string_literal: true

require 'spec_helper'

describe UsageDataByMonthService do
  describe '#execute' do
    it 'returns a hash of hashes of counts' do
      travel_to(Time.utc(2017, 3, 1, 12, 0, 0)) do
        create(:usage_data, edition: 'EE', recorded_at: Time.utc(2017, 2, 28))
        create(:usage_data, edition: nil, recorded_at: Time.utc(2017, 2, 14))
        create(:usage_data, :eep, recorded_at: Time.utc(2017, 2, 1))
        create(:usage_data, :eep, recorded_at: Time.utc(2017, 1, 31))

        expect(subject.execute).to eq('2017-02' => { 'EE' => 2, 'EEP' => 1 },
                                      '2017-01' => { 'EEP' => 1 })
      end
    end
  end
end
