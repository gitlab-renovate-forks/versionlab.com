# frozen_string_literal: true

require 'spec_helper'

describe UsagePing::StatsParser, :usage_data do
  describe '#execute' do
    context 'with correct incoming stats format' do
      subject { described_class.new(test_usage_fixed_epics_stats) }

      it 'correct formats without formatting' do
        expect(subject.execute[:stats].values).to be_all { |e| e.is_a?(Integer) }
      end

      it 'stats are correct without formatting' do
        expect(subject.execute[:stats]).to match(test_usage_fixed_epics_stats)
      end

      it 'has no stats for the user_preferences - CE case' do
        local_params = { user_preferences: {} }
        subject = described_class.new(local_params)

        expect(subject.execute[:stats]).to match(test_fixed_flattened_stats)
      end
    end

    context 'with incorrect incoming stats format' do
      subject { described_class.new(test_usage_broken_stats) }

      it 'formatted to correct formats' do
        expect(subject.execute[:stats].values).to be_all { |e| e.is_a?(Integer) }
      end

      it 'stats formatted to be numeric' do
        expect(subject.execute[:stats]).to match(test_usage_fixed_epics_stats)
      end

      context 'with incorrect epics stats' do
        subject { described_class.new(test_usage_broken_stats2) }

        it 'formatted to correct formats' do
          expect(subject.execute[:stats].values).to be_all { |e| e.is_a?(Integer) }
        end

        it 'stats formatted to be numeric' do
          expect(subject.execute[:stats]).to include(test_usage_fixed_stats2)
        end
      end
    end
  end
end
