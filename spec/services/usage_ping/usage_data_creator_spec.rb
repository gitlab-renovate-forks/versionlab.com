# frozen_string_literal: true

require 'spec_helper'

describe UsagePing::UsageDataCreator do
  describe '#execute!' do
    let(:host) { create(:host, url: 'example.com') }
    let(:attributes) do
      {
        edition: 'CE',
        uuid: '1',
        host: host
      }
    end

    subject { described_class.new(attributes) }

    it 'creates a usage_data record' do
      expect { subject.execute! }.to change(UsageData, :count).by(1)
    end

    it 'returns the usage_data record' do
      expect(subject.execute!).to eq UsageData.last
    end

    it 'is not able to create a usage data record' do
      allow(UsageData).to receive(:create!).and_raise('boom')

      expect { subject.execute! }.to raise_error('boom')
    end
  end
end
