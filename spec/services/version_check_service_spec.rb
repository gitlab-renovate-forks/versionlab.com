# frozen_string_literal: true

require 'spec_helper'

describe VersionCheckService do
  let(:url) { 'http://gitlab.example.com' }

  describe '#execute' do
    let(:gitlab_info) { Base64.urlsafe_encode64({ version: '9.4.0', type: 'CE' }.to_json) }

    subject(:check_service) { described_class.new(gitlab_info) }

    shared_examples 'works as expected' do |result_message = 'success'|
      it "returns status: '#{result_message}'" do
        expect(check_service.execute[:status]).to eq(result_message)
      end

      it 'does not create a new host record' do
        expect { check_service.execute }.not_to change { Host.count }
      end

      it 'does not update current_host_stats' do
        expect { check_service.execute }.not_to change { CurrentHostStat.last }
      end
    end

    shared_examples 'critical vulnerability' do |result|
      it "returns critical_vulnerability: #{result}" do
        expect(check_service.execute[:critical_vulnerability]).to eq(result)
      end
    end

    context 'when everything is ok' do
      let!(:version) { create(:version, version: '9.4.0') }

      context 'when hostname does not exist' do
        include_examples 'works as expected', 'success'
      end

      context 'when hostname exists' do
        let(:host) { create(:host, url: 'gitlab.example.com') }

        context 'when there is no usage data for the host' do
          include_examples 'works as expected'
        end

        context 'when there is a usage data record for the host' do
          before do
            create(:usage_data, host: host)
          end

          include_examples 'works as expected'
        end
      end
    end

    context 'when version is problematic' do
      let!(:version) { create(:version, version: '16.1.5') }
      let(:gitlab_info) { Base64.urlsafe_encode64({ version: '16.1.5', type: 'CE' }.to_json) }

      shared_examples 'successful check version without DB persistence' do
        it "returns status: 'success'" do
          expect(check_service.execute[:status]).to eq('success')
        end

        it "does not create or update DB records" do
          expect { check_service.execute }.not_to change { Host.count }
          expect { check_service.execute }.not_to change { CurrentHostStat.last }
        end
      end

      context 'when hostname does not exist' do
        include_examples 'successful check version without DB persistence'
      end

      context 'when hostname exists' do
        let(:host) { create(:host, url: 'gitlab.example.com') }

        include_examples 'successful check version without DB persistence'

        context 'when there is only affected version checks for the host' do
          before do
            create(:current_host_stat, host: host)
          end

          include_examples 'successful check version without DB persistence'
        end

        context 'when there is a usage data record for the host' do
          before do
            create(:usage_data, host: host)
          end

          include_examples 'successful check version without DB persistence'
        end
      end
    end

    context 'when hostname is not a valid url' do
      let(:url) { 'something' }

      subject(:check_service) { described_class.new(gitlab_info) }

      it 'returns status: "failed"' do
        expect(check_service.execute[:status]).to eq('failed')
      end

      it 'does not create a new host record' do
        expect { check_service.execute }.not_to change { Host.count }
      end
    end

    context 'when the version is not up to date but not vulnerable' do
      before do
        create(:version, version: '10.8.9', vulnerability_type: :not_vulnerable)
      end

      include_examples 'works as expected', 'warning'
      include_examples 'critical vulnerability', false
    end

    context 'when the version has vulnerability_type of non_critical' do
      before do
        create(:version, version: '9.4.0', vulnerability_type: :non_critical)
      end

      include_examples 'works as expected', 'danger'
      include_examples 'critical vulnerability', false
    end

    context 'when the version has vulnerability_type of critical' do
      before do
        create(:version, version: '9.4.0', vulnerability_type: :critical)
      end

      include_examples 'works as expected', 'danger'
      include_examples 'critical vulnerability', true
    end

    context 'when the gitlabinfo is not a valid JSON' do
      subject(:check_service) { described_class.new(Base64.urlsafe_encode64('some string')) }

      it 'returns status: "failed"' do
        expect(check_service.execute[:status]).to eq('failed')
      end

      it 'logs the error' do
        expect(Rails.logger).to receive(:warn)

        check_service.execute
      end
    end

    context 'when token is expired' do
      let(:expired_token) do
        'eyJlcnJvciI6ImludmFsaWRfdG9rZW4iLCJlcnJvcl9kZXNjcmlwdGlvbiI6IlRva2Vu' \
          'IGlzIGV4cGlyZWQuIFlvdSBjYW4gZWl0aGVyIGRvIHJlLWF1dGhvcml6YXRpb24gb3Ig' \
          'dG9rZW4gcmVmcmVzaC4ifQ=='
      end

      subject(:check_service) { described_class.new(expired_token) }

      it 'returns status: "failed"' do
        expect(check_service.execute[:status]).to eq('failed')
      end
    end

    describe 'current_host_stat' do
      let(:hostname) { URI.parse(url).host }

      context 'when a current_host_stat does exist' do
        it 'does not update current host stats' do
          create(:current_host_stat, url: hostname, version: '0.0.0')

          expect { check_service.execute }.not_to change { CurrentHostStat.last }
        end
      end

      context 'when a current_host_stat does not exist yet' do
        it 'has no record created' do
          expect { check_service.execute }.not_to change { CurrentHostStat.count }
        end

        context 'when no url is provided' do
          let(:url) { nil }

          it 'has no record created' do
            expect { check_service.execute }.not_to change { CurrentHostStat.count }
          end
        end

        it 'has no version check info' do
          check_service = described_class.new(nil)

          expect { check_service.execute }.not_to change { CurrentHostStat.count }
        end
      end
    end
  end
end
