# frozen_string_literal: true

require 'simplecov'
require 'sidekiq/testing'
require 'webmock'
require 'webmock/rspec'

SimpleCov.start 'rails' do
  add_filter 'vendor'
end

Sidekiq::Testing.fake!

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path('../config/environment', __dir__)
require 'rspec/rails'
require 'shoulda/matchers'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join('spec/support/**/*.rb')].sort.each { |f| require f }

quality_level = Quality::TestLevel.new

RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # enabled aggregating failures globally
  # https://relishapp.com/rspec/rspec-core/docs/expectation-framework-integration/aggregating-failures#enable-failure-aggregation-globally-using-%60define-derived-metadata%60
  config.define_derived_metadata do |meta|
    meta[:aggregate_failures] = true
  end

  config.define_derived_metadata(file_path: %r{/spec/.+_spec\.rb\z}) do |metadata|
    location = metadata[:location]

    metadata[:level] = quality_level.level_for(location)

    # Do not overwrite migration if it's already set
    unless metadata.key?(:migration)
      metadata[:migration] = true if metadata[:level] == :migration
    end

    # Do not overwrite schema if it's already set
    unless metadata.key?(:schema)
      metadata[:schema] = :latest if quality_level.background_migration?(location)
    end
  end

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false
  config.infer_spec_type_from_file_location!

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"

  config.include LoginHelpers, type: :feature
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include UsageDataHelpers, :usage_data
  config.include MigrationsHelpers, :migration
  config.include FactoryBot::Syntax::Methods
  config.include ExpectOffense
  config.include RailsHelpers
  config.include ActiveSupport::Testing::TimeHelpers
  config.include StubENV

  # The :each scope runs "inside" the example, so this hook ensures the DB is in the
  # correct state before any examples' before hooks are called. This prevents a
  # problem where `ScheduleIssuesClosedAtTypeChange` (or any migration that depends
  # on background migrations being run inline during test setup) can be broken by
  # altering Sidekiq behavior in an unrelated spec like so:
  #
  # around do |example|
  #   Sidekiq::Testing.fake! do
  #     example.run
  #   end
  # end

  config.before do
    Sidekiq::Worker.clear_all
  end

  config.before(:context, :migration) do
    schema_migrate_down!
  end

  # Each example may call `migrate!`, so we must ensure we are migrated down every time
  config.before(:each, :migration) do
    schema_migrate_down!
  end

  config.after(:context, :migration) do
    schema_migrate_up!
  end
end

ActiveRecord::Migration.maintain_test_schema!

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end

# needed for the browser testing to be allowed by WebMock local for local and CI needs chromedriver
# Let's ask the gem who the possible browser URL's it may use for version checking
driver_urls = Webdrivers::Common.subclasses.map(&:base_url)

WebMock.disable_net_connect!(allow_localhost: true, allow: driver_urls)
