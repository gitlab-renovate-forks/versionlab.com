# frozen_string_literal: true

require 'spec_helper'

describe 'metrics/index.html.haml' do
  let(:instance_counts) do
    {
      "CE" => double(instances: 5, licenses: 0, ratio: 0),
      "EE Free" => double(instances: 5, licenses: 0, ratio: 0),
      "EES" => double(instances: 5, licenses: 2, ratio: 0),
      "EE" => double(instances: 10, licenses: 5, ratio: 0),
      "EEP" => double(instances: 100, licenses: 2, ratio: 0),
      "EEU" => double(instances: 1000, licenses: 4, ratio: 0)
    }
  end

  let(:usage_statistics) do
    {
      "groups" => 1,
      "projects" => 1
    }
  end

  before do
    assign(:usage_statistics, usage_statistics)
  end

  context 'when we are displaying all correctly' do
    before do
      assign(:instance_counts, instance_counts)
    end

    it 'shows all the editions' do
      ee_regex = UsageData::EE_EDITIONS.join(' licenses:.*')

      render

      expect(rendered).to have_content(/CE hosts:.*#{ee_regex}/)
    end

    it 'has a link for scheduling pushes to salesforce' do
      render

      expect(rendered)
        .to have_link('View totals by month and schedule pushes to Salesforce', href: '/usage_data/months')
    end
  end

  context 'when there are editions that are not tracked' do
    before do
      rogue_edition = { 'EE Rogue' => double(instances: 0, licenses: 0, ratio: 0) }
      assign(:instance_counts, instance_counts.merge(rogue_edition))
    end

    it 'does not show editions that are not tracked yet' do
      render

      expect(rendered).not_to have_content(/EE Rogue/)
    end
  end
end
